use std::fmt::Display;
use std::ops::Deref;

use crate::{
    dm::DM,
    parser::{Atom, AtomString, Token},
    scene::Node,
};

/// Receives notifications as a Action goes through its life cycle
pub trait Rule<'a> {
    /// A Rule has to declare the verbs it is interested in
    /// hearing about.  This allows the Parser to pick them up.  First
    /// verb is the cannonical form that the others parse into.
    fn verbs(&self) -> &[&[&str]] {
        Default::default()
    }
    /// If a Rule returns a non-empty Vec, processing of the
    /// action starts over with the new ones used in place of the
    /// original (or previously refined) Action.  All Rules get
    /// another chance to refine the new Action.  It would be an
    /// error to return an already-seen Action.  Should not change
    /// game state.  All refinements are collated.
    #[allow(unused_variables)]
    fn refine(&self, dm: &DM<'a>, action: &Action<'a>) -> Vec<Action<'a>> {
        Default::default()
    }
    /// If any Rule returns Some, the Action is rejected, and all
    /// returned Strings are shown to the user.  Should ensure action
    /// is valid or return an error message if not.  Should not change
    /// game state.
    #[allow(unused_variables)]
    fn veto(&self, dm: &DM<'a>, action: &Action<'a>) -> Option<String> {
        Default::default()
    }
    /// Attempt to execute the action.  Assume action is valid.  The
    /// attempt can fail or have unintended consequences.  Tell the
    /// witnesses what changes occur by calling Node::tell on the
    /// parent of affected nodes.
    // "do" is a keyword, so:
    #[allow(unused_variables)]
    fn frobnicate(&self, dm: &DM<'a>, action: &Action<'a>) {}
    /// Executed after all frobnicate phases have completed.  Should
    /// not directly change game state but can return additional
    /// Actions to be processed. XXX: How does it know what
    /// happened?  It is not deterministic.  Can only react to the
    /// attempt whether it fails or succeeds.  Maybe need to have
    /// concept of failure?
    #[allow(unused_variables)]
    fn react(&self, dm: &DM<'a>, action: &Action<'a>) -> Vec<Action<'a>> {
        Default::default()
    }
}

/// A Rule you can plug implementations into
pub struct DynamicRule<'a> {
    pub refine: fn(&DM, &Action<'a>) -> Vec<Action<'a>>,
    pub veto: fn(&DM, &Action<'a>) -> Option<String>,
    pub frobnicate: fn(&DM, &Action<'a>),
    pub react: fn(&DM, &Action<'a>) -> Vec<Action<'a>>,
}

impl Default for DynamicRule<'_> {
    fn default() -> Self {
        DynamicRule {
            refine: |_, _| Default::default(),
            veto: |_, _| Default::default(),
            frobnicate: |_, _| Default::default(),
            react: |_, _| Default::default(),
        }
    }
}

#[derive(Debug, Clone)]
pub struct Phrase<T>(pub Token, T);
impl<T> Display for Phrase<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        self.0.fmt(f)
    }
}
impl<SelfT, T: Display> PartialEq<T> for Phrase<SelfT> {
    fn eq(&self, other: &T) -> bool {
        let other = Token::from(other.to_string());
        self.0 == other
    }
}

macro_rules! phrase_type {
    ($type:ident, $marker:ident) => {
        #[derive(Debug, Clone)]
        pub struct $marker;
        pub type $type = Phrase<$marker>;
        impl From<&str> for $type {
            fn from(value: &str) -> Self {
                Self(Token::from(value), $marker)
            }
        }
        impl From<Token> for $type {
            fn from(value: Token) -> Self {
                Self(value, $marker)
            }
        }
    };
}

phrase_type!(Preposition, PrepositionKind);
phrase_type!(Verb, VerbKind);
phrase_type!(Pronoun, PronounKind);
phrase_type!(Adverb, AdverbKind);

impl Default for Verb {
    fn default() -> Self {
        Self::from("frobnicate")
    }
}

/// A Noun is either a direct reference to a Node or a Pronoun that
/// needs to be resolved in context.  A Noun can be used in place of a
/// Node most of the time, and it will dereference itself.  But if it
/// is a Noun::Pronoun, this will panic.
#[derive(Debug, Clone, PartialEq)]
pub enum Noun<'a> {
    Node(Node<'a>),
    Pronoun(Pronoun),
}

impl<'a> Deref for Noun<'a> {
    type Target = Node<'a>;
    fn deref(&self) -> &Self::Target {
        if let Noun::Node(node) = self {
            node
        } else {
            panic!("Dereferenced pronoun")
        }
    }
}

impl<'a> From<&Node<'a>> for Noun<'a> {
    fn from(node: &Node<'a>) -> Self {
        Noun::Node(node.clone())
    }
}

impl<'a> PartialEq<Node<'a>> for Noun<'a> {
    fn eq(&self, other: &Node<'a>) -> bool {
        match self {
            Noun::Pronoun(_) => false,
            Noun::Node(node) => node == other,
        }
    }
}

impl<'a> PartialEq<Noun<'a>> for Node<'a> {
    fn eq(&self, other: &Noun<'a>) -> bool {
        other == self
    }
}

impl<'a> Display for Noun<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Noun::Pronoun(pronoun) => f.write_fmt(format_args!("{pronoun}")),
            Noun::Node(node) => f.write_fmt(format_args!("{node}")),
        }
    }
}

#[derive(Debug, Default, Clone, PartialEq)]
pub struct Action<'a> {
    pub actor: Node<'a>,
    pub verb: Verb,
    pub object: Option<Noun<'a>>,
    // eg "quietly"
    pub adverb: Option<Adverb>,
    // eg "with the sword"
    pub indirect: Option<(Preposition, Noun<'a>)>,
    // eg when verb == "ask", "type", etc.
    // ...maybe also for orders to give them custom representations
    pub text: Option<String>,
    // Text that couldn't be parsed
    pub garbage: Option<String>,
    // eg "goblin, bite me"
    pub order: Option<Box<Action<'a>>>,
}

/// Quick and dirty, don't expect too much.  Can't set actor because
/// it isn't expresed in the AtomString; caller must set.
impl<'a> From<AtomString<'a>> for Action<'a> {
    fn from(value: AtomString<'a>) -> Self {
        let mut action = Action::default();
        let mut cursor = value.0.iter().peekable();
        // Get the first verb
        while let Some(atom) = cursor.peek() {
            match atom {
                Atom::Adverb(v) => {
                    // Replace any existing adverb
                    action.adverb = Some(v.clone());
                    cursor.next();
                }
                Atom::Verb(v) => {
                    action.verb = v.clone();
                    cursor.next();
                    break;
                }
                _ => {
                    // Leave whatever this is in the iterator
                    break;
                }
            }
        }
        let mut predicate = &Preposition::from("with");
        for atom in cursor {
            match atom {
                Atom::Adverb(v) => {
                    // Replace any existing adverb; could extend (TODO)
                    action.adverb = Some(v.clone());
                }
                Atom::Verb(_) => {
                    // Can't have two verbs, neither get applied
                    action.verb = "[ invalid ]".into();
                }
                Atom::Predicate(v) => {
                    // Remember this in case we see a subject
                    predicate = v;
                }
                Atom::Noun(v) => {
                    // First noun is the object, second is the
                    // indirect object, third is an error (ignore)
                    if action.object.is_none() {
                        action.object = Some(v.into());
                    } else if action.indirect.is_none() {
                        action.indirect = Some((predicate.clone(), v.into()));
                    } else {
                        // No way to signal a failure here
                        log::error!("TODO: deal with bad atom strings");
                    }
                }
                Atom::Pronoun(v) => {
                    let pronoun = Noun::Pronoun(v.clone());
                    if action.object.is_none() {
                        action.object = Some(pronoun);
                    } else if action.indirect.is_none() {
                        action.indirect = Some((predicate.clone(), pronoun));
                    } else {
                        // No way to signal a failure here
                        log::error!("TODO: deal with bad atom strings");
                    }
                }
                Atom::Garbage(v) => {
                    let text = action.garbage.get_or_insert(String::new());
                    for token in v {
                        if !text.is_empty() {
                            text.push(' ');
                        }
                        text.push_str(token);
                    }
                }
            }
        }

        // Pull text out of garbage if it starts with a colon or
        // semicolon.  This isn't correct but it might be close enough
        // for the short term.
        if let Some(garbage) = action.garbage.as_ref() {
            if garbage.starts_with(": ") || garbage.starts_with("; ") {
                action.text = Some(garbage[2..].to_string());
                action.garbage = None;
            }
        }

        action
    }
}

impl Display for Action<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.describe().as_str())
    }
}

impl<'a> Action<'a> {
    pub fn describe(&self) -> String {
        format!(
            "« {}I {}{}{}. »",
            if self.actor.is_pc() {
                Default::default()
            } else {
                format!("{}: ", self.actor.the())
            },
            self.describe_verb(),
            self.describe_object(&self.actor),
            self.describe_subject_or_order(&self.actor),
        )
    }
    fn describe_verb(&self) -> String {
        if let Some(modifier) = self.adverb.as_ref() {
            format!("{modifier} {0}", self.verb)
        } else {
            format!("{0}", self.verb)
        }
    }
    fn describe_object(&self, speaker: &Node<'a>) -> String {
        if let Some(object) = self.object.as_ref() {
            format!(
                " {}",
                if object == speaker {
                    "me".to_string()
                } else {
                    object.the()
                }
            )
        } else {
            Default::default()
        }
    }
    fn describe_subject_or_order(&self, speaker: &Node<'a>) -> String {
        if let Some(text) = self.text.as_ref() {
            format!(": {text}")
        } else if let Some(order) = self.order.as_ref() {
            format!(
                " to {}{}{}",
                order.describe_verb(),
                order.describe_object(speaker),
                order.describe_subject_or_order(speaker)
            )
        } else if let Some((predicate, subject)) = self.indirect.as_ref() {
            let subject_name = if subject == speaker {
                "me".to_string()
            } else {
                subject.the()
            };
            if let Some(text) = self.text.as_ref() {
                format!(" {predicate} {subject_name}, \"{text}\"")
            } else {
                format!(" {predicate} {subject_name}",)
            }
        } else {
            Default::default()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::character::Character;

    use super::*;
    #[test]
    fn describe() {
        let mut scene = Node::default();
        let pc = &scene.new_child(|data| {
            data.desc.set_short("PC");
            data.desc.set_article("the");
            data.character = Some(Character::Pc {
                stats: Default::default(),
            })
        });
        assert_eq!(
            Action {
                actor: pc.clone(),
                verb: "frob".into(),
                ..Default::default()
            }
            .describe(),
            "« I frob. »"
        );
        let goblin = &scene.new_child(|data| {
            data.desc.set_short("goblin");
            data.desc.set_article("the");
        });
        assert_eq!(
            Action {
                actor: pc.clone(),
                verb: "frob".into(),
                object: Some(goblin.into()),
                ..Default::default()
            }
            .describe(),
            "« I frob the goblin. »"
        );
        let wand = &scene.new_child(|data| {
            data.desc.set_short("wand");
            data.desc.set_article("the");
        });
        assert_eq!(
            Action {
                actor: pc.clone(),
                verb: "frob".into(),
                object: Some(goblin.into()),
                indirect: Some(("with".into(), wand.into())),
                ..Default::default()
            }
            .describe(),
            "« I frob the goblin with the wand. »"
        );
        assert_eq!(
            Action {
                actor: pc.clone(),
                verb: "frob".into(),
                adverb: Some("grimly".into()),
                object: Some(goblin.into()),
                indirect: Some(("with".into(), wand.into())),
                ..Default::default()
            }
            .describe(),
            "« I grimly frob the goblin with the wand. »"
        );
        assert_eq!(
            Action {
                actor: pc.clone(),
                adverb: Some("grimly".into()),
                verb: "order".into(),
                object: Some(goblin.into()),
                order: Some(Box::new(Action {
                    actor: goblin.clone(),
                    verb: "frob".into(),
                    adverb: Some("lightly".into()),
                    object: Some(pc.into()),
                    indirect: Some(("with".into(), wand.into())),
                    ..Default::default()
                })),
                ..Default::default()
            }
            .describe(),
            "« I grimly order the goblin to lightly frob me with the wand. »"
        );
        assert_eq!(
            Action {
                actor: goblin.clone(),
                adverb: Some("grimly".into()),
                verb: "order".into(),
                object: Some(pc.into()),
                order: Some(Box::new(Action {
                    actor: pc.clone(),
                    verb: "frob".into(),
                    adverb: Some("lightly".into()),
                    object: Some(goblin.into()),
                    indirect: Some(("with".into(), wand.into())),
                    ..Default::default()
                })),
                ..Default::default()
            }
            .describe(),
            "« the goblin: I grimly order you to lightly frob me with the wand. »"
        );
        assert_eq!(
            Action {
                text: Some("FROB ME, YOU WORM".into()),
                actor: goblin.clone(),
                adverb: Some("grimly".into()),
                verb: "order".into(),
                object: Some(pc.into()),
                order: Some(Box::new(Action {
                    actor: pc.clone(),
                    verb: "frob".into(),
                    adverb: Some("lightly".into()),
                    object: Some(goblin.into()),
                    indirect: Some(("with".into(), wand.into())),
                    ..Default::default()
                })),
                ..Default::default()
            }
            .describe(),
            "« the goblin: I grimly order you: FROB ME, YOU WORM. »"
        );
    }
}
