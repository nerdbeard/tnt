use std::{
    cell::RefCell,
    collections::VecDeque,
    fmt::{Debug, Display},
    sync::atomic::{AtomicUsize, Ordering::Relaxed},
};

use tinyrand::{RandRange, Seeded, StdRand};

use crate::{
    action::{Action, Rule},
    character::Character,
    dice::{Cup, Roller},
    parser::{AtomParser, AtomString, CoreParser, Input, Parser, Token},
    scene::{Node, NodeData},
    ui::{UserInterface, UI},
};

#[derive(Debug, Clone)]
pub enum GameOver {
    Error,
    Lost,
    Quit,
    Restarted,
    Won,
}

#[derive(Clone)]
pub enum SystemCommand {
    Quit(GameOver),
    Message(String),
}

impl Debug for SystemCommand {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("Command::")?;
        match self {
            SystemCommand::Message(input) => f.debug_tuple("Invalid").field(input).finish(),
            SystemCommand::Quit(gameover) => f.debug_tuple("Quit").field(gameover).finish(),
        }
    }
}

#[derive(Default)]
pub struct DM<'a> {
    pub ui: UserInterface,
    scene: RefCell<Option<Node<'a>>>,
    pc: RefCell<Option<Node<'a>>>,
    // We want to own the Rules, but since dyn Rule is not
    // sized, we put them in Boxes.
    rules: Vec<Box<dyn Rule<'a>>>,
    game_over: RefCell<Option<String>>,
    rand: RefCell<StdRand>,
    tick: AtomicUsize,
}

impl Debug for DM<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("DM")
            .field("ui", &..)
            .field("scene", &self.scene)
            .field("pc", &self.pc)
            .field("rules", &..)
            .field("game_over", &self.game_over)
            .field("rand", &..)
            .field("tick", &self.tick)
            .finish()
    }
}

impl Roller for crate::dm::DM<'_> {
    fn roll_dice(&self, cup: &Cup) -> Vec<i16> {
        let cup: Vec<_> = cup.dice().collect();
        if cup.is_empty() {
            self.msg(
                "  [ The Dungeon Master rolls no dice from an empty cup. \
		 ]  Ah, interesting.",
            );
            vec![]
        } else {
            let rolls: Vec<i16> = cup
                .iter()
                .map(|sides| {
                    (self.rand.borrow_mut().next_range(0..*sides) + 1)
                        .try_into()
                        .unwrap()
                })
                .collect();
            self.dm_msg(format!(
                "rolls a {}",
                if rolls.len() == 1 {
                    format!("die: {}/{}", rolls[0], cup[0])
                } else {
                    let sum: i16 = rolls.iter().sum();
                    let rolls = rolls
                        .iter()
                        .zip(cup)
                        .map(|(roll, sides)| format!("{roll}/{sides}"))
                        .collect::<Vec<_>>()
                        .join(" ");
                    format!("cup of dice: {rolls} = {sum}")
                }
            ));
            rolls
        }
    }
}

impl<'a> Parser<'a> for DM<'a> {
    fn parse(&self, tokens: &[crate::parser::Token]) -> AtomString<'a> {
        // Get the core parser
        let mut atom_parsers: Vec<Box<&dyn AtomParser>> = vec![Box::new(&CoreParser)];
        // Add on the rules
        for listener in &self.rules {
            atom_parsers.push(Box::new(listener));
        }
        // And all the visible nodes
        let nodes: Vec<_> = self.player().parent().unwrap().visible().collect();
        for node in &nodes {
            atom_parsers.push(Box::new(node));
        }
        // Delegate to the slice's Parser implementation
        atom_parsers.as_slice().parse(tokens)
    }
}

impl<'a> DM<'a> {
    /// Initialized with all the basic features
    pub fn new(seed: u64) -> Self {
        let mut dm: DM = Default::default();
        dm.core_lib();
        dm.set_seed(seed);
        dm
    }

    /// Pick a specific future
    pub fn set_seed(&self, seed: u64) {
        *self.rand.borrow_mut() = StdRand::seed(seed);
    }

    /// Initialize the DM with the core library
    pub fn core_lib(&mut self) {
        self.rules.extend(crate::core::init_dm())
    }

    pub fn reset_ui(&mut self) {
        self.ui = Default::default();
    }

    fn default_scene(&self) -> Node<'a> {
        let scene = Node::default();
        scene.data(|data| {
            data.desc.set_article("the");
            data.desc.set_short("Dungeon Master");
        });
        scene
    }

    pub fn scene(&self) -> Node<'a> {
        if self.scene.borrow().is_some() {
            self.scene.borrow().clone().unwrap()
        } else {
            *self.scene.borrow_mut() = Some(self.default_scene());
            self.scene()
        }
    }

    /// Ask the player a question; prompt for and return any line of
    /// input.  Prompt is expected not to contain newlines.
    pub async fn ask(&self, prompt: &str) -> Result<String, crate::ui::Error> {
        self.ui.ask(prompt).await
    }

    pub async fn ask_yn(&self, prompt: &str) -> bool {
        self.ui.ask_yn(prompt).await
    }

    /// Called to indicate that the player did something
    /// time-consuming
    pub fn tick(&self) {
        self.tick.store(self.tick.fetch_add(1, Relaxed), Relaxed)
    }

    pub fn action(&mut self, action: Action<'a>) {
        self.nl();
        // Temporarily take ownership of the rules. If we borrowed
        // instead, there would be an implicit borrow against *self,
        // causing neccessary further mutable borrows of self to fail.
        let rules = std::mem::take(&mut self.rules);
        let mut queue = VecDeque::from([action]);
        while let Some(action) = queue.pop_front() {
            if self.game_over.borrow().is_some() {
                break;
            }
            // Sanity check -- Scene root is used for system actions
            assert!(
                action.actor.parent().is_none() || action.actor.borrow().character.is_some(),
                "Bad actor: {:?}",
                action.actor
            );
            // Try to refine this action
            let refined: Vec<_> = rules
                .iter()
                .flat_map(|listener| listener.refine(self, &action))
                .collect();
            if !refined.is_empty() {
                for action in refined {
                    // There is no "extend_front"?
                    queue.push_front(action);
                }
                continue;
            }

            // Garbage in action
            if let Some(garbage) = action.garbage.as_ref() {
                self.msg(format!(
                    "  One failed to understand what you meant by, \
		     \"{garbage}\"."
                ));
                continue;
            }

            // Too many verbs
            if action.verb == "[ invalid ]" {
                self.msg(
                    "  Perhaps one misunderstood, but there seemed to \
			  be more than one verb in that sentence.",
                );
                continue;
            }

            // No verb was found or refined
            if action.verb == "frobnicate" {
                self.msg(
                    "  One failed to understand the verb in what you just \
		     said.  If you spelled it correctly, you may be able \
		     to use a synonym, or it might just not be important \
		     to the adventure.",
                );
                continue;
            }

            // It has passed through refinement unchanged; announce it
            if let Some(room) = action.actor.parent() {
                // Don't indent
                self.nl();
                room.tell(self, &action.describe());
            }

            // Try to veto this action
            let vetoed: Vec<_> = rules
                .iter()
                .flat_map(|listener| listener.veto(self, &action))
                .collect();
            if !vetoed.is_empty() {
                self.nl();
                action
                    .actor
                    .tell(self, &("  ".to_owned() + &vetoed.join("  ")));
            } else {
                // No vetos, apply it
                for listener in &rules {
                    listener.frobnicate(self, &action);
                }
                // Finally, react to it and enqueue responses
                queue.extend(
                    rules
                        .iter()
                        .flat_map(|listener| listener.react(self, &action))
                        .collect::<Vec<_>>(),
                );
            }
            if !queue.is_empty() {
                self.nl();
            }
        }
        // Return ownership of Rules
        self.rules = rules;
    }

    pub fn game_over(&self, reason: String) {
        *self.game_over.borrow_mut() = Some(reason)
    }

    pub async fn game_step(&mut self, input: Input) -> Result<(), GameOver> {
        let mut sentences = input.sentences().peekable();
        if sentences.peek().is_none() {
            self.msg("...");
        }
        for sentence in sentences {
            let tokens = sentence.tokens();
            match self.parse_command(tokens.as_slice()) {
                Some(SystemCommand::Message(msg)) => {
                    self.nl();
                    if !msg.is_empty() {
                        self.msg(format!("  {}", &msg));
                    }
                }
                Some(SystemCommand::Quit(GameOver::Quit)) => {
                    if self.ask_yn("Really quit?").await {
                        return Err(GameOver::Quit);
                    } else {
                        self.msg("  Okay.");
                    }
                }
                Some(SystemCommand::Quit(GameOver::Restarted)) => {
                    if self.ask_yn("Really restart?").await {
                        return Err(GameOver::Restarted);
                    } else {
                        self.msg("  Okay.");
                    }
                }
                Some(SystemCommand::Quit(quit)) => {
                    return Err(quit);
                }
                None => {
                    self.player_action(crate::action::Action::from(self.parse(tokens.as_slice())))
                        .await?;
                }
            };
        }
        Ok(())
    }

    /// Sets action.actor.  Advances time if neccesary.
    async fn player_action(&mut self, mut action: Action<'a>) -> Result<(), GameOver> {
        action.actor = self.player();
        self.action(action);

        // The action took time; emit ticks to allow other characters
        // to perform time-consuming actions as well.
        let mut tick = self.tick.load(Relaxed);
        while tick > 0 {
            tick -= 1;
            self.tick.store(tick, Relaxed);
            self.action(Action {
                actor: self.scene(),
                verb: ".tick".into(),
                ..Default::default()
            });
        }

        // The action has set the game_over flag

        if self.game_over.borrow().is_some() {
            let game_over = self.game_over.borrow_mut().take().unwrap();
            self.nl();
            self.msg(format!("\n[ The game is over: {game_over} ]\n"));
            loop {
                if self.ask_yn("\nRestart?").await {
                    return Err(GameOver::Restarted);
                }
                if self.ask_yn("\nQuit?").await {
                    return Err(GameOver::Quit);
                }
            }
        }

        // The action and its consequences have all resolved without
        // ending the game
        Ok(())
    }

    fn prompt(&self) -> &'static str {
        "\n> "
    }
    /// Get a line of raw input from the user after displaying a prompt
    pub async fn read_input(&self) -> Result<String, crate::ui::Error> {
        self.nl();
        self.ask(self.prompt()).await
    }

    /// Sniff out any system commands before dispatching it
    fn parse_command(&self, input: &[Token]) -> Option<SystemCommand> {
        match input
            .iter()
            .map(|atom| atom.as_str())
            .collect::<Vec<_>>()
            .as_slice()
        {
            [] => Some(SystemCommand::Message("".to_string())),
            ["quit"] => Some(SystemCommand::Quit(GameOver::Quit)),
            ["restart"] => Some(SystemCommand::Quit(GameOver::Restarted)),
            _ => None,
        }
    }
    pub fn parse_name(&self, words: &[&str]) -> Option<(usize, Node<'a>)> {
        let mut visible = self
            .player()
            .descendants()
            .chain(
                self.player()
                    .parent()
                    .expect("PC doesn't have a parent")
                    .children(),
            )
            .map(|node| (node.borrow().match_name(words), node.clone()))
            .collect::<Vec<_>>();
        visible.sort_by_key(|item| item.0);
        let last = visible.last().unwrap();
        if last.0 > 0 {
            Some((last.0, last.1.clone()))
        } else {
            None
        }
    }
    /// Push a newline if the buffer does not already end in one
    pub fn nl(&self) {
        self.ui.nl()
    }
    pub fn dm_msg<T: Display>(&self, text: T) {
        self.msg(format!("  [ The Dungeon Master {text} ]"))
    }
    pub fn msg<T: Display>(&self, text: T) {
        self.ui.msg(&text.to_string())
    }
    pub async fn msg_flush(&self) {
        self.ui.msg_flush().await
    }
    pub fn player(&self) -> Node<'a> {
        self.make_player(|_| ())
    }
    /// Calls [init] if a player does not already exist
    pub fn make_player(&self, init: impl Fn(&mut NodeData)) -> Node<'a> {
        if self.pc.borrow().is_some() {
            self.pc.borrow().clone().unwrap()
        } else {
            *self.pc.borrow_mut() = Some(self.scene().new_child(|pc| {
                pc.statuses.insert("pc");
                pc.statuses.insert("invisible");
                pc.statuses.insert("transparent"); // You can refer to the contents of the PC
                pc.desc.set_description("You are here (of course).");
                pc.desc.set_initial("Wherever you go, there you are.");
                pc.character = Some(Character::Pc {
                    stats: Default::default(),
                });
                init(pc);
            }));
            self.player()
        }
    }
    pub fn push_rule(&mut self, value: Box<dyn Rule<'a>>) {
        self.rules.push(value)
    }

    pub async fn game_loop(&mut self) -> GameOver {
        let pc = self.player();
        self.nl();
        self.msg(format!("  {}", pc.parent().unwrap().describe(&pc)));
        loop {
            if let Err(game_over) = self.game_loop_iter().await {
                return game_over;
            }
        }
    }

    async fn game_loop_iter(&mut self) -> Result<(), GameOver> {
        if let Some(input) = self.read_valid_input().await {
            self.game_step(input).await
        } else {
            log::error!("Could not get valid user input");
            Err(GameOver::Error)
        }
    }

    async fn read_valid_input(&self) -> Option<Input> {
        let mut was_eof = false;
        loop {
            match self.read_input().await {
                Err(err) => {
                    log::error!("Reading user input: {err:?}");
                    if was_eof {
                        break None;
                    } else {
                        was_eof = true;
                    }
                }
                Ok(input) => {
                    break Some(input.into());
                }
            }
        }
    }

    pub fn roll(&self, dice: usize, sides: usize, adds: isize) -> isize {
        {
            let cup = Cup::default()
                .d(dice.try_into().unwrap(), sides.try_into().unwrap())
                .plus(adds.try_into().unwrap());
            self.roll_cup(&cup).into()
        }
    }
}

#[cfg(test)]
mod tests {
    use std::{cell::Cell, rc::Rc};

    use super::*;
    #[test]
    fn parse_command() {
        let dm = DM::default();
        let mut input = vec![];
        let command = dm.parse_command(&input);
        assert!(
            matches!(&command, Some(SystemCommand::Message(invalid)) if invalid.is_empty()),
            "command: {command:?}"
        );
        input.push("quit".into());
        let command = dm.parse_command(&input);
        assert!(matches!(command, Some(SystemCommand::Quit(GameOver::Quit))));
    }

    #[test]
    fn dispatcher() {
        #[derive(Default)]
        struct TestRule {
            frobnicate: Rc<Cell<bool>>,
            react: Rc<Cell<bool>>,
            refine: Rc<Cell<bool>>,
            veto: Rc<Cell<bool>>,
        }
        impl<'a> Rule<'a> for TestRule {
            fn refine(&self, _: &DM, _: &Action<'a>) -> Vec<Action<'a>> {
                self.refine.set(true);
                vec![]
            }
            fn veto(&self, _: &DM, _: &Action<'a>) -> Option<String> {
                self.veto.set(true);
                None
            }
            fn frobnicate(&self, _: &DM, _: &Action<'a>) {
                self.frobnicate.set(true);
            }
            fn react(&self, _: &DM, _: &Action<'a>) -> Vec<Action<'a>> {
                self.react.set(true);
                Default::default()
            }
        }
        let test_listener: TestRule = Default::default();
        let (frobnicate, react, refine, veto) = (
            test_listener.frobnicate.clone(),
            test_listener.react.clone(),
            test_listener.refine.clone(),
            test_listener.veto.clone(),
        );
        let mut dm: DM = Default::default();
        dm.rules.push(Box::new(test_listener));
        dm.action(Action {
            actor: dm.player(),
            verb: "test".into(),
            ..Default::default()
        });

        assert_eq!(
            (refine.get(), veto.get(), frobnicate.get(), react.get()),
            (true, true, true, true)
        );
    }
}
