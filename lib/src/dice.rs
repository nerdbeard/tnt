use std::collections::HashMap;

/// Builder for a dice roll -- a cup of dice
#[derive(Default, Clone, PartialEq, Eq)]
pub struct Cup {
    dice: HashMap<u16, u16>,
    adds: i16,
}

/// idk why
impl Cup {
    pub fn plus(self, adds: i16) -> Self {
        Self {
            adds: self.adds + adds,
            ..self
        }
    }
    pub fn d(self, dice: u16, sides: u16) -> Self {
        Self {
            dice: {
                let mut cup = self.dice;
                let dice = *cup.get(&sides).unwrap_or(&0) + dice;
                cup.insert(sides, dice);
                cup
            },
            ..self
        }
    }
    pub fn dice(&self) -> DieIterator {
        self.into()
    }
}

pub struct DieIterator(Vec<u16>);

impl Iterator for DieIterator {
    type Item = u16;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop()
    }
}

impl From<&Cup> for DieIterator {
    fn from(value: &Cup) -> Self {
        let mut cup = vec![];
        for (side, dice) in &value.dice {
            for _ in 0..*dice {
                cup.push(*side);
            }
        }
        DieIterator(cup)
    }
}

/// No whammies!
pub trait Roller {
    /// Give sum including adds.
    fn roll_cup(&self, cup: &Cup) -> i16 {
        self.roll_dice(cup).iter().sum::<i16>() + cup.adds
    }
    /// Itemize rolls.  Does not include adds!
    fn roll_dice(&self, cup: &Cup) -> Vec<i16>;
}

/// Has no state.  Returns the expected average value for the entire
/// cup.
struct NullRoller;
impl Roller for NullRoller {
    fn roll_dice(&self, cup: &Cup) -> Vec<i16> {
        let mut rolls = vec![];
        let mut total = 0;
        // Accumulate error from previous rolls
        for sides in cup.dice() {
            total += sides;
            let roll = total >> 1;
            total -= roll << 1;
            rolls.push(roll.try_into().unwrap());
        }
        rolls
    }
}

// XXX I don't think stringification is the right solution here but it
// is a working solution
// XXX also I would rather errors happen at compile time, not runtime!
#[macro_export]
macro_rules! roll {
    ($dm:expr, $spec:tt + $adds:expr) => {{
        pub const INVALID_SIDES: &str = "Invalid side specification";
        pub const INVALID_DICE: &str = "Invalid dice specification";
        let adds: isize = $adds;
        let spec = stringify!($spec);
        let (dice, sides) = if let Some(spec) = spec.strip_prefix("d") {
            (1, spec.parse::<usize>().expect(INVALID_SIDES))
        } else {
            let mut spec = spec.split('d');
            let dice = spec
                .next()
                .expect(INVALID_DICE)
                .parse::<usize>()
                .expect(INVALID_DICE);
            let sides = spec
                .next()
                .expect(INVALID_SIDES)
                .parse::<usize>()
                .expect(INVALID_SIDES);
            if spec.next().is_some() {
                panic!("Garbage after side specification");
            }
            (dice, sides)
        };
        $dm.roll(dice, sides, adds)
    }};
    ($dm:expr, $spec:tt) => {
        roll!($dm, $spec + 0)
    };
    ($dm:expr, $spec:tt - $adds:expr) => {
        roll!($dm, $spec + -$adds)
    };
}

#[cfg(test)]
mod tests {
    use crate::dm::DM;

    /// If it compiles, it passes.
    #[test]
    fn roll() {
        let dm = &DM::default();
        roll!(dm, 3d4);
        roll!(dm, d100);
        roll!(dm, 4d20 - 10);
        roll!(dm, d6 + 6);
    }
}
