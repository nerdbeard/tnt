use std::cell::RefCell;

use crate::{
    action::{Action, Noun, Rule},
    dm::DM,
    scene::{Node, Nodes},
    string::Munge,
};

pub fn init_dm<'a>() -> Vec<Box<dyn Rule<'a>>> {
    vec![
        // alphabetical
        Box::new(Describable),
        Box::new(Ignition),
        Box::new(Item),
        Box::new(Room),
    ]
}

/// Pronouns
#[derive(Default)]
pub struct Pronouns<'a> {
    it: RefCell<Node<'a>>,
}

impl<'a> Rule<'a> for Pronouns<'a> {
    fn refine(&self, dm: &DM<'a>, action: &Action<'a>) -> Vec<Action<'a>> {
        if action.actor != dm.player() {
            return vec![];
        }
        let mut refined = false;
        let mut new_action = action.clone();
        let nouns = [
            // Indirect first because we remember the last one we see
            new_action.indirect.as_mut().map(|(_, noun)| noun),
            new_action.object.as_mut(),
        ];
        for noun in nouns.into_iter().flatten() {
            match noun {
                Noun::Node(node) => {
                    // Remember seeing this node
                    *self.it.borrow_mut() = node.clone();
                }
                Noun::Pronoun(_) => {
                    // Replace in the Action
                    *noun = Noun::Node(self.it.borrow().clone());
                    refined = true;
                }
            }
        }
        if refined {
            vec![new_action]
        } else {
            vec![]
        }
    }
}

#[macro_export]
macro_rules! listen {
    {$action:ident, $($rule:pat => $code:block),+} => {
        match ($action.verb.0.as_str(), &$action.object, &$action.indirect) {
	    $($rule => {$code},)*
		_ => Default::default()
	}
    };
}

/// Rooms and their interconnecting exits
pub struct Room;
impl<'a> Rule<'a> for Room {
    fn verbs(&self) -> &[&[&str]] {
        &[&["enter", "go", "explore", "walk", "run", "exit", "use"]]
    }
    fn refine(&self, _: &DM<'a>, action: &Action<'a>) -> Vec<Action<'a>> {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            ("frobnicate", Some(object), None) if object.has_status("enterable") => vec![{
                let mut action = action.clone();
                action.verb = "enter".into();
                action
            }],
            _ => vec![],
        }
    }
    fn veto(&self, _: &DM<'a>, action: &Action<'a>) -> Option<String> {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            ("enter", Some(object), _) if !object.has_status("enterable") => {
                Some("That's not enterable.".into())
            }
            ("enter", Some(object), Some((pred, indirect))) if object.has_status("enterable") => {
                Some(format!("You lost one at {pred} {indirect}."))
            }
            _ => None,
        }
    }
    fn frobnicate(&self, dm: &DM<'a>, action: &Action<'a>) {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            ("enter", Some(object), None) if object.has_status("enterable") => {
                let target = object
                    .borrow()
                    .target
                    .clone()
                    .expect("Exit did not have target set");
                let actor = action.actor.the().capitalize();
                let left = action.actor.parent().unwrap();
                left.tell(dm, &format!("\n  {actor} enter {object}."));
                target.tell(dm, &format!("\n  {actor} arrives from {left}."));
                action.actor.set_parent(&target);
                dm.tick();
            }
            _ => {}
        }
    }
    fn react(&self, _: &DM<'a>, action: &Action<'a>) -> Vec<Action<'a>> {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            ("enter", _, None) if !action.actor.parent().unwrap().has_status("examined") => {
                vec![Action {
                    actor: action.actor.clone(),
                    verb: "look around".into(),
                    ..Default::default()
                }]
            }
            _ => vec![],
        }
    }
}

/// Things that can be examined by the player and described by the
/// game
pub struct Describable;
impl<'a> Rule<'a> for Describable {
    fn verbs(&self) -> &[&[&str]] {
        &[
            &["look around", "look", "l"],
            &["examine", "look at", "look", "ex", "x", "l"],
        ]
    }
    fn refine(&self, _: &DM<'a>, action: &Action<'a>) -> Vec<Action<'a>> {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            // Help out the parser find the right sense in the case of
            // the bare verb
            ("examine", None, None) => vec![{
                let mut action = action.clone();
                action.verb = "look around".into();
                action
            }],
            _ => vec![],
        }
    }
    fn veto(&self, _: &DM<'a>, action: &Action<'a>) -> Option<String> {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            // No object -- no longer possible
            ("examine", None, _) => unreachable!("Should have been refined"),

            // Over-specified
            ("look around", Some(_), _) => {
                Some("You can [look around], but you can't look around something.".into())
            }

            // Way over-specified
            ("examine" | "look around", _, Some(_)) => Some("Don't make it weird.".into()),

            _ => None,
        }
    }
    fn frobnicate(&self, dm: &DM<'a>, action: &Action<'a>) {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            ("examine", Some(object), None) => {
                action.actor.tell(dm, &format!("\n  {}", object.examine()));
            }
            ("look around", None, None) => action.actor.tell(
                dm,
                &format!("\n  {}", action.actor.parent().unwrap().examine()),
            ),
            _ => {}
        }
    }
}

/// Things that can be picked up by the player, carried in their
/// inventory, dropped in a room's contents, placed in a container,
/// etc., and related actions.
pub struct Item;
impl<'a> Rule<'a> for Item {
    fn verbs(&self) -> &[&[&str]] {
        &[
            &["take", "get"],
            &["drop", "put down", "set down"],
            &["insert", "put"],
            &["take inventory", "i", "inv", "inventory"],
            &["open"],
            &["close"],
        ]
    }
    fn veto(&self, _: &DM, action: &Action) -> Option<String> {
        let verb = action.verb.0.as_str();
        match (verb, &action.object, &action.indirect) {
            // Missing object
            ("open" | "close" | "take" | "drop" | "insert", None, None) => {
                Some(format!("What do you want to {}?", action.verb))
            }

            // Not an item
            ("take" | "drop" | "insert", Some(object), _) if !object.has_status("item") => {
                Some(format!(
                    "{} is not something you can {}.",
                    object.the().capitalize(),
                    action.verb
                ))
            }

            // Not openable
            ("open" | "close", Some(object), _) if !object.has_status("openable") => Some(format!(
                "{} is not something you can {}.",
                object.the().capitalize(),
                action.verb
            )),

            // Already open
            ("open", Some(object), _)
                if object.has_status("openable") && object.has_status("open") =>
            {
                Some(format!("{} is already open.", object.the().capitalize(),))
            }

            // Already closed
            ("close", Some(object), _)
                if object.has_status("openable") && !object.has_status("open") =>
            {
                Some(format!("{} is already closed.", object.the().capitalize(),))
            }

            // Already taken
            ("take", Some(object), _) if object.parent().as_ref() == Some(&action.actor) => {
                Some(format!("You already have {}.", object.the()))
            }

            // Not present for taking
            ("take", Some(object), _)
                if !action
                    .actor
                    .parent()
                    .unwrap()
                    .visible()
                    // First item is actor's parent,
                    // which would be weird, but what is actor
                    // doing in an item, anyway?
                    .skip(1)
                    .any(|node| &node == object) =>
            {
                Some(format!(
                    "{} is not any place you can take it from.",
                    object.the().capitalize()
                ))
            }

            // Not carried for dropping
            ("drop", Some(object), _) if object.parent().as_ref().unwrap() != &action.actor => {
                Some(format!("You are not carrying {}.", object.the()))
            }

            // No indirect to insert into
            ("insert", _, None) => Some(format!(
                "What do you want to insert {} into?",
                action.object.as_ref().unwrap().the()
            )),

            // Not inserting into a container
            ("insert", _, Some((preposition, indirect))) if !indirect.has_status("container") => {
                Some(format!(
                    "{} is not something you can {} items {}.",
                    indirect.the().capitalize(),
                    action.verb,
                    preposition
                ))
            }

            // Over-specified inventory
            ("take inventory", Some(object), _) => Some(format!(
                "You can only take inventory of your personal possesions; you \
		 can't take inventory of {object}, you can simply [take \
		 inventory]; [i] or [inv] for short."
            )),

            // Looks good
            _ => None,
        }
    }
    fn frobnicate(&self, dm: &DM, action: &Action) {
        match action.verb.0.as_str() {
            "take" => {
                action.object.as_ref().unwrap().set_parent(&action.actor);
                action.actor.parent().unwrap().tell(
                    dm,
                    &format!(
                        "\n  {} {} {}.",
                        action.actor.the().capitalize(),
                        if action.actor.is_pc() {
                            "take"
                        } else {
                            "takes"
                        },
                        action.object.as_ref().unwrap().the()
                    ),
                );
                dm.tick();
            }
            "drop" => {
                action
                    .object
                    .as_ref()
                    .unwrap()
                    .set_parent(&action.actor.parent().unwrap());
                action.actor.parent().unwrap().tell(
                    dm,
                    &format!(
                        "\n  {} {} {}.",
                        action.actor.the().capitalize(),
                        if action.actor.is_pc() {
                            "drop"
                        } else {
                            "drops"
                        },
                        action.object.as_ref().unwrap().the()
                    ),
                );
                dm.tick();
            }
            "insert" => {
                action
                    .object
                    .as_ref()
                    .unwrap()
                    .set_parent(&action.indirect.as_ref().unwrap().1);
                action.actor.parent().unwrap().tell(
                    dm,
                    &format!(
                        "\n  {} {} {} {} {}.",
                        action.actor.the().capitalize(),
                        if action.actor.is_pc() { "put" } else { "puts" },
                        action.object.as_ref().unwrap().the(),
                        action.indirect.as_ref().unwrap().0,
                        action.indirect.as_ref().unwrap().1.the()
                    ),
                );
                dm.tick();
            }
            "take inventory" => {
                let items = dm.player().borrow().children().list_nodes();
                dm.msg(format!("\n  You are carrying {items}."));
            }
            "open" => {
                let object = action.object.as_ref().unwrap();
                object.set_status("open");
                let actor = action.actor.the().capitalize();
                action
                    .actor
                    .tell(dm, &format!("\n  {actor} open {object}."));
                dm.tick();
            }
            "close" => {
                let object = action.object.as_ref().unwrap();
                object.clear_status("open");
                let actor = action.actor.the().capitalize();
                action
                    .actor
                    .tell(dm, &format!("\n  {actor} close {object}."));
                dm.tick();
            }
            _ => {}
        }
    }
    fn react(&self, _: &DM, _action: &Action<'a>) -> Vec<Action<'a>> {
        Default::default()
    }
    fn refine(&self, _: &DM, _action: &Action<'a>) -> Vec<Action<'a>> {
        Default::default()
    }
}

struct Ignition;

impl<'a> Rule<'a> for Ignition {
    fn refine(&self, dm: &DM, action: &Action<'a>) -> Vec<Action<'a>> {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            ("light", Some(object), Some((prep, indirect)))
                if object.has_status("ignitable")
                    && indirect.has_status("igniter")
                    && prep != &"with" =>
            {
                dm.msg("\n  (You correct your grammar.)");
                // Fix it to use the expected preposition
                let mut action = action.clone();
                action.indirect = Some(("with".into(), indirect.clone()));
                return vec![action];
            }
            _ => {}
        }
        vec![]
    }
    fn verbs(&self) -> &[&[&str]] {
        &[
            &["light", "ignite", "burn", "turn on"],
            &["extinguish", "douse", "turn off", "blow out"],
        ]
    }

    fn veto(&self, _: &DM, action: &Action<'a>) -> Option<String> {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            // Bare verb
            ("light" | "extinguish", None, _) => {
                Some(format!("{} what?", action.verb.0.capitalize()))
            }

            // Already lit
            ("light", Some(object), _) if object.has_status("aflame") => {
                Some("It is already lit.".into())
            }

            // Not lit
            ("extinguish", Some(object), _) if !object.has_status("aflame") => {
                Some("It is not burning.".into())
            }

            // Bad object
            ("light" | "extinguish", Some(object), _) if !object.has_status("ignitable") => {
                Some("That can't be lit.".into())
            }

            // Not holding object
            ("light" | "extinguish", Some(object), _)
                if object.parent().unwrap() != action.actor =>
            {
                Some(format!("You don't have {object}."))
            }

            // Under-specified
            ("light", Some(object), None) => {
                Some(format!("You must light {object} [with] [something]."))
            }

            // Wrong indirect object
            ("light", Some(object), Some((_, indirect))) if !indirect.has_status("igniter") => {
                Some(format!("You can light {object}, but not with {indirect}."))
            }

            // Not holding lighter
            ("light", _, Some((_, indirect))) if indirect.parent().unwrap() != action.actor => {
                Some(format!("You're not holding {indirect}."))
            }

            // Holding unlit lighter
            ("light", _, Some((_, indirect))) if !indirect.has_status("aflame") => {
                Some(format!("{} is unlit.", indirect.the().capitalize(),))
            }

            // The actually valid case or not my concern
            _ => None,
        }
    }

    fn frobnicate(&self, dm: &DM<'a>, action: &Action<'a>) {
        match (action.verb.0.as_str(), &action.object, &action.indirect) {
            ("light", Some(object), Some((with, indirect))) => {
                if with == &"with" {
                    object.set_status("aflame");
                    action.actor.parent().unwrap().tell(
                        dm,
                        &format!(
                            "\n  {} light {object} {with} {indirect}.",
                            action.actor.the().capitalize()
                        ),
                    );
                };
                dm.tick();
            }
            ("extinguish", Some(object), None) => {
                object.clear_status("aflame");
                action.actor.parent().unwrap().tell(
                    dm,
                    &format!(
                        "\n  {} extinguish {object}.",
                        action.actor.the().capitalize()
                    ),
                );
                dm.tick();
            }
            _ => {}
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn take_item() {
        let rule = Item;
        let dm: DM = Default::default();
        let mut action: Action = Default::default();
        let player = dm.player();
        let scene = dm.scene();

        // Use the scene as a room; make it transparent to
        // Node::visible
        scene.set_status("transparent");

        player.set_parent(&scene);
        action.actor = player.clone();

        // Ignores uninteresting action
        assert!(matches!(rule.veto(&dm, &action), None));

        // Can't take without object
        action.verb = "take".into();
        assert!(matches!(rule.veto(&dm, &action), Some(_)));

        // Can't pick up non-item
        let item = dm.scene().new_child(|_| {});
        action.object = Some((&item).into());
        assert!(matches!(rule.veto(&dm, &action), Some(_)));

        // Can pick up item
        item.set_status("item");
        assert!(
            matches!(rule.veto(&dm, &action), None),
            "Was: {}",
            rule.veto(&dm, &action).unwrap_or_default()
        );

        // Does pick up
        assert_eq!(item.parent().unwrap(), player.parent().unwrap());
        rule.frobnicate(&dm, &action);
        assert_eq!(item.parent().unwrap(), player);

        // Can no longer pick up
        assert!(
            matches!(rule.veto(&dm, &action), Some(_)),
            "Was not vetoed: {action}"
        );
    }
}
