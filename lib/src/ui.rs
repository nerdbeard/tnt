use std::{
    sync::{
        atomic::{AtomicBool, Ordering::Relaxed},
        Arc, Mutex as StdMutex,
    },
    time::Duration,
};

use async_trait::async_trait;
use tokio::sync::{mpsc, Mutex};
use wasm_timer::Instant;

// Input ////////////////////////////////////////

pub type InputSender = mpsc::UnboundedSender<InputEvent>;
pub type InputReceiver = mpsc::UnboundedReceiver<InputEvent>;

pub fn input_event_stream() -> (InputSender, InputReceiver) {
    mpsc::unbounded_channel()
}

#[derive(Debug, PartialEq)]
pub enum InputEvent {
    // A simple printable ascii character
    Char(u8),
    // Time has passed
    Idle,
    // A symbolic keystroke
    Key(KeyCode),
}

#[derive(Debug, PartialEq)]
pub enum KeyCode {
    Return,
    ArrowUp,
    ArrowDown,
    ArrowLeft,
    ArrowRight,
}

// Output ////////////////////////////////////////

pub type OutputSender = mpsc::UnboundedSender<OutputEvent>;
pub type OutputReceiver = mpsc::UnboundedReceiver<OutputEvent>;

pub fn output_event_stream() -> (OutputSender, OutputReceiver) {
    mpsc::unbounded_channel()
}

#[derive(Debug)]
pub enum OutputEvent {
    // A simple printable utf-8 character or a newline
    Char(char),
    // Basic terminal control
    Control(ControlCode),
}

#[derive(Debug)]
pub enum ControlCode {
    // Ring terminal bell/flash screen
    Bell,
    // Bold/reverse/intense
    Bright,
    // Clear the terminal screen
    Clear,
    // Delete previous character
    Backspace,
    // End bright mode
    Plain,
    // Delete back to most recent newline
    Kill,
}

// UserInterface ////////////////////////////////////////

/// This presents methods to a DM to interact with an input producer
/// and an output consumer.

#[derive(Debug)]
pub struct UserInterface {
    // UI reads and writes to this pair
    output_sender: OutputSender,
    input_receiver: Mutex<InputReceiver>,
    // Exposes this pair for application to read and write
    connection: Option<(InputSender, OutputReceiver)>,
    // Implement our terminal UI machinery
    msg_buffer: Arc<StdMutex<String>>,
    was_nl: AtomicBool,
}

impl UserInterface {
    pub fn connect(&mut self) -> Option<(InputSender, OutputReceiver)> {
        self.connection.take()
    }

    // Send a string of text immediately, bypassing the text wrap etc
    fn write(&self, text: &str) {
        for character in text.chars() {
            self.write_char(character);
        }
    }

    fn write_char(&self, character: char) {
        self.output_sender
            .send(match character {
                '' => OutputEvent::Control(ControlCode::Bell),
                '' => OutputEvent::Control(ControlCode::Clear),
                '' => OutputEvent::Control(ControlCode::Backspace),
                _ => OutputEvent::Char(character),
            })
            .unwrap();
    }

    // Send a screen of text with a delay between each character
    async fn print_screen(&self, screen: &[&str]) {
        use crate::BAUD;
        let started = Instant::now();
        const MICROS: u64 = 10000000 / BAUD;
        for (count, character) in screen.join("\n").chars().enumerate() {
            self.write_char(character);
            let now = Instant::now();
            let then = started + Duration::from_micros(count as u64 * MICROS);
            if now < then {
                futures_timer::Delay::new(then - now).await;
            }
        }
    }

    async fn more(&self) {
        self.write("\n[MORE]");
        self.read_key().await.unwrap();
        self.write("");
    }
}

impl Default for UserInterface {
    fn default() -> UserInterface {
        let (input_sender, input_receiver) = input_event_stream();
        let (output_sender, output_receiver) = output_event_stream();
        UserInterface {
            output_sender,
            input_receiver: Mutex::new(input_receiver),
            connection: Some((input_sender, output_receiver)),
            msg_buffer: Default::default(),
            was_nl: true.into(),
        }
    }
}

#[async_trait]
impl UI for UserInterface {
    /// Buffer some text for display
    fn msg(&self, text: &str) {
        self.msg_buffer.lock().unwrap().push_str(text);
        self.was_nl.store(text.ends_with('\n'), Relaxed);
    }

    /// Push a newline if the buffer does not already end in one
    fn nl(&self) {
        if !self.was_nl.load(Relaxed) {
            self.msg("\n");
        }
    }

    /// Get a single keystroke without echoing it
    async fn read_key(&self) -> Result<u8, Error> {
        log::trace!("read_key");
        while let Some(event) = self.input_receiver.lock().await.recv().await {
            match event {
                InputEvent::Char(c) => {
                    log::trace!("Got key: {c}");
                    return Ok(c);
                }
                InputEvent::Idle => {
                    log::trace!("Idle");
                }
                _ => {
                    log::error!("Unhandled event: {event:?}");
                }
            }
        }
        Err(Error)
    }

    /// Send all pending output to the user
    async fn msg_flush(&self) {
        let mut spaces = String::new();
        let fill = {
            let mut buf = self.msg_buffer.lock().unwrap();
            let fill = textwrap::fill(&buf, textwrap::Options::new(40));
            while buf.ends_with(' ') || buf.ends_with('\n') {
                spaces.push(buf.pop().unwrap());
            }
            buf.clear();
            fill
        };
        let lines = fill.lines().collect::<Vec<_>>();
        let mut screens = lines.chunks_exact(20);
        for screen in screens.by_ref() {
            self.print_screen(screen).await;
            self.more().await;
        }
        self.print_screen(screens.remainder()).await;
        for char in spaces.chars().rev() {
            self.write_char(char);
        }
    }

    /// Ask the player a question; prompt for and return any line of input
    async fn ask(&self, prompt: &str) -> Result<String, Error> {
        self.msg(prompt);
        self.msg_flush().await;
        let mut buf = String::new();
        while let Ok(character) = self.read_key().await {
            // Echo and flush
            let mut converted_character: char = character.into();
            converted_character = match converted_character {
                '\r' => '\n',
                '\u{7f}' => '',
                _ => converted_character,
            };
            self.write(&format!("{converted_character}"));
            match character {
                // Enter
                b'\n' | b'\r' => {
                    break;
                }

                // Backspace
                8u8 | 127u8 => {
                    if buf.pop().is_none() {
                        // Nothing to rub out, replace prompt
                        self.write(&format!("{}", prompt.chars().last().unwrap()));
                    };
                }

                // Buffer the character
                _ => {
                    buf.push(converted_character);
                }
            }
        }
        Ok(buf)
    }
}

// Legacy ////////////////////////////////////////

#[derive(Debug)]
pub struct Error;

#[async_trait]
pub trait UI: Send {
    /// Buffer some text for display
    fn msg(&self, text: &str);
    /// Push a newline if the buffer does not already end in one
    fn nl(&self);
    /// Get a single keystroke without echoing it
    async fn read_key(&self) -> Result<u8, Error>;
    /// Send all pending output to the user
    async fn msg_flush(&self);
    /// Ask the player a question; prompt for and return any line of input
    async fn ask(&self, prompt: &str) -> Result<String, Error>;
    async fn ask_yn(&self, prompt: &str) -> bool {
        self.msg(&format!("{prompt} [y/n]: "));
        {
            let msg_flush = self.msg_flush();
            msg_flush.await;
        }
        let value = loop {
            match {
                let read_key = self.read_key();
                read_key.await
            }
            .unwrap()
            {
                b'y' | b'Y' => {
                    break true;
                }
                b'n' | b'N' => {
                    break false;
                }
                _ => continue,
            }
        };
        self.msg(&format!(" {}.", if value { "Yes" } else { "No" }));
        value
    }
}

pub struct Null;
#[async_trait]
impl UI for Null {
    fn msg(&self, _: &str) {}
    fn nl(&self) {}
    async fn read_key(&self) -> Result<u8, Error> {
        Err(Error)
    }
    async fn msg_flush(&self) {}
    async fn ask(&self, _: &str) -> Result<String, Error> {
        Err(Error)
    }
}
