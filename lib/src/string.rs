use crate::parser::Token;

pub trait Munge {
    fn tokenize(&self) -> Vec<Token>;
    fn plural(&self) -> String;
    fn capitalize(&self) -> String;
}

impl Munge for str {
    fn tokenize(&self) -> Vec<Token> {
        self.split_whitespace()
            // Fold to lowercase
            .map(Token::from)
            .collect::<Vec<_>>()
    }
    fn plural(&self) -> String {
        format!(
            "{self}{}",
            self.chars()
                .last()
                .map(|c| match c {
                    's' => {
                        "es"
                    }
                    'S' => {
                        "ES"
                    }
                    'A'..='Z' => {
                        "S"
                    }
                    _ => {
                        "s"
                    }
                })
                .unwrap_or("")
        )
    }
    fn capitalize(&self) -> String {
        let mut s = String::new();
        if self.is_empty() {
            return s;
        }
        // This is safe because we know it is at least one char long
        s.push(self.chars().next().unwrap().to_ascii_uppercase());
        s.push_str(&self[1..]);
        s
    }
}

impl PartialEq<&str> for Token {
    fn eq(&self, other: &&str) -> bool {
        other.eq_ignore_ascii_case(self.0.as_str())
    }
}
