use crate::stats::{CharacterStats, MonsterStats};

#[derive(Debug, Hash, PartialEq, Eq)]
pub enum Character {
    Pc { stats: CharacterStats },
    Npc { stats: CharacterStats },
    Monster { stats: MonsterStats },
}
