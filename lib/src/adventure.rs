use std::time::Duration;

use crate::{
    action::{Action, Rule},
    core::Pronouns,
    dm::DM,
    roll,
    scene::Node,
    stats::{CharacterStats, MonsterStats},
    string::Munge,
    ui::UI,
};

pub fn init(dm: &mut DM<'static>) {
    // Pronoun Rule has state and needs to be inserted first, so
    // we handle it specially.  Unfortunately, this is the best I have
    // come up with so far due to lifetime issues I haven't figured
    // out yet.
    dm.push_rule(Box::<Pronouns<'_>>::default());
    let mut scene = dm.scene();

    let inside = &mut scene.new_child(|data| {
        data.desc.set_short("Inside the Dark Cave Mouth");
        data.statuses.insert("transparent");
    });

    let goblin = &mut inside.new_child(|data| {
        data.desc.set_article("a");
        data.desc.set_short("goblin");
        data.desc
            .set_initial("A goblin is not at all pleased to meet you.");
        data.desc
            .set_description("A goblin is looking at you with loathing.");
        data.desc.set_long(
            "He is smaller than you are.  In fact, he resembles a \
	     shrunken old man.  However, it is not his first adventure.",
        );
        data.character = Some(crate::character::Character::Monster {
            stats: MonsterStats {
                health: crate::stats::Health { hp: 6, damage: 0 },
            },
        });
    });
    struct Goblin<'a> {
        goblin: Node<'a>,
    }
    impl<'a> Rule<'a> for Goblin<'a> {
        fn react(&self, dm: &DM<'a>, action: &Action<'a>) -> Vec<Action<'a>> {
            let player = dm.player();
            if action.verb == ".tick" && player.parent() == self.goblin.parent() {
                vec![Action {
                    actor: self.goblin.clone(),
                    verb: "attack".into(),
                    object: Some((&player).into()),
                    ..Default::default()
                }]
            } else {
                vec![]
            }
        }
    }
    dm.push_rule(Box::new(Goblin {
        goblin: goblin.clone(),
    }));

    // Actions that relate to this "adventure", ie game module
    struct Adventure;
    impl<'a> Rule<'a> for Adventure {
        fn verbs(&self) -> &[&[&str]] {
            &[&[
                "manipulate space and time",
                "save",
                "restore",
                "load",
                "undo",
                "oops",
            ]]
        }
        fn veto(&self, _dm: &DM<'a>, command: &Action<'a>) -> Option<String> {
            match command.verb.0.as_str() {
                "manipulate space and time" => {
                    Some("Ha ha!  One would be amused to see that.".into())
                }
                _ => None,
            }
        }
    }
    dm.push_rule(Box::new(Adventure));

    let mut outside = scene.new_child(|outside| {
        outside
            .desc
            .set_description("You are in the hills away from town.");
        outside.desc.set_short("Outside the Cave");
        outside.desc.set_initial(
            "You are outside a cave in the hills not too far from your hick \
             town.  You've heard rumours of treasure and monsters, so you \
             have come a-looting.  You're totally Lawful, though.\
             \n  Some even say that the long lost Chalice of Tabarnak can be \
	     found somewhere around here!  More likely you might find Bagel, \
	     a local bandit you believe to skulk in these hills between \
	     raids on your town.",
        );
        outside.statuses.insert("transparent");
    });

    let cave_mouth = &outside.new_child(|cave| {
        cave.desc.set_initial(
            "The dark cave mouth could be entered if you have light.\
             ",
        );
        cave.desc.set_short("dark cave entrance");
        cave.desc.set_article("a");
        cave.desc.set_long(
            "You are absolutely certain to be eaten by a grue \
             if you enter like this.",
        );
        cave.names.insert("cave");
        cave.statuses.insert("enterable");
    });
    cave_mouth.borrow_mut().target = Some(inside.clone());

    let cave_exit = inside.new_child(|data| {
        data.desc.set_short("cave exit");
        data.desc.set_article("a");
        data.statuses.insert("enterable");
    });
    cave_exit.borrow_mut().target = Some(outside.clone());

    let mut pc = dm.make_player(|pc| {
        pc.desc.set_short("PC");
        pc.desc.set_article("the");
        pc.names.insert("me");
        let pc_stats = CharacterStats {
            str: 17.into(),
            dex: 11.into(),
            int: 9.into(),
            con: 16.into(),
            wis: 8.into(),
            chr: 14.into(),
            health: crate::stats::Health { hp: 8, damage: 0 },
        };
        pc.character = Some(crate::character::Character::Pc { stats: pc_stats });
    });
    pc.set_parent(&outside); // Didn't call new_child

    let lamp = pc.new_child(|node| {
        node.statuses.insert("ignitable");
        node.statuses.insert("item");
        node.desc.set_short("lantern");
        node.desc.set_article("the");
        node.names.insert("light");
        node.names.insert("lamp");
        node.desc.set_long("It is unlit.");
    });

    struct CaveGrue<'a> {
        cave_mouth: Node<'a>,
        outside: Node<'a>,
        lamp: Node<'a>,
    }
    impl<'a> Rule<'a> for CaveGrue<'a> {
        fn refine(&self, dm: &DM<'a>, command: &Action<'a>) -> Vec<Action<'a>> {
            match (command.verb.0.as_str(), &command.object, &command.indirect) {
                ("enter", Some(object), None)
                    if object == &self.cave_mouth
                        && !object.has_status("examined")
                        && !self.lamp.has_status("aflame") =>
                {
                    dm.msg(
                        "  [ The DM secretly rolls some dice. ]  Some \
                            instinct of self-preservation causes you to \
                            examine the cave mouth before entering it.\n",
                    );
                    vec![Action {
                        actor: command.actor.clone(),
                        verb: "examine".into(),
                        object: Some((&self.cave_mouth).into()),
                        ..Default::default()
                    }]
                }
                _ => vec![],
            }
        }
        fn frobnicate(&self, _: &DM<'a>, command: &Action<'a>) {
            match (command.verb.0.as_str(), &command.object, &command.indirect) {
                ("examine", Some(object), None) if object == &self.cave_mouth => {
                    self.cave_mouth.set_status("examined");
                }
                ("light", Some(object), _) if object == &self.lamp => {
                    self.cave_mouth.borrow_mut().desc.set_long(
                        "You are reasonably unlikely to be eaten by a grue \
                         if you keep your lantern lit and with you.",
                    );
                }
                // Update cave desc again when lamp extinguishes
                ("extinguish", Some(object), _) if object == &self.lamp => {
                    self.cave_mouth.borrow_mut().desc.set_long(
                        "One probably doesn't have to tell you, but you \
                         are absolutely certain to be eaten by a grue \
                         if you enter like this.",
                    );
                }
                _ => (),
            }
        }
        fn react(&self, dm: &DM<'a>, command: &Action<'a>) -> Vec<Action<'a>> {
            if let Some(room) = command.actor.parent() {
                if command.actor.is_pc()
                // PC isn't in the safe room
                && room != self.outside
                // Lamp isn't on or isn't visible
                && !(self.lamp.has_status("aflame")
                     && room.visible().any(|node| node == self.lamp))
                {
                    // KILL THE PC!! HAHAHAHAAAAA!!
                    dm.msg(
                        "  You have been eaten by a grue.  One does not know \
                	 what you expected.",
                    );
                    dm.game_over("eaten by a grue".into());
                };
            }
            vec![]
        }
    }
    dm.push_rule(Box::new(CaveGrue {
        cave_mouth: cave_mouth.clone(),
        outside: outside.clone(),
        lamp: lamp.clone(),
    }));

    let mut tinderbox = pc.new_child(|node| {
        node.statuses.insert("item");
        node.statuses.insert("openable");
        node.statuses.insert("transparent");
        node.desc.set_article("the");
        node.names.insert("tinderbox");
        node.names.insert("tinder box");
        node.names.insert("tinder");
        node.names.insert("box");
    });

    let punk = tinderbox.new_child(|node| {
        node.statuses.insert("igniter");
        node.statuses.insert("item");
        node.desc.set_short("unlit punk");
        node.desc.set_article("an");
        node.desc.set_long(
            "A sappy splinter of wood that will keep a small flame \
             for not much more than a minute.  You keep a supply \
             of them in your tinderbox.",
        );
        node.names.insert("punk");
        node.names.insert("match");
    });

    // Set anything not handled by the core lib, so I don't have to
    // duplicate text anywhere.
    fn close_tinderbox(tinderbox: &Node) {
        tinderbox.data(|data| {
            data.desc.set_short("tinderbox (closed)");
            data.desc.set_long(
                "It is an AGSC Basic Tinderbox.  Pretty much everyone \
                 knows how to use and refill one of these for personal fire \
                 making needs, but sometimes it takes a couple of minutes \
                 to get a lit punk out of it.  Particularily in the \
                 dark...",
            );
        })
    }

    // Set anything not handled by the core lib, so I don't have to
    // duplicate text anywhere.  Except this comment.
    fn open_tinderbox(tinderbox: &Node) {
        tinderbox.data(|data| {
            data.desc.set_short("tinderbox (open)");
            data.desc.set_long(
                "It is an AGSC Basic Tinderbox.  It \
                 contains a supply of unlit punks, a sparker built into \
                 the case, the actual tinder, and some inner chambers for \
                 organizing and processing everything.  You can light a \
                 punk, but it might take a minute.",
            );
        })
    }

    /// The special cases for the lamp, punk and tinderbox; just add to Adventure?
    struct LampRule<'a> {
        lamp: Node<'a>,
        punk: Node<'a>,
        tinderbox: Node<'a>,
    }

    impl<'a> Rule<'a> for LampRule<'a> {
        fn refine(&self, _: &DM<'a>, command: &Action<'a>) -> Vec<Action<'a>> {
            match (command.verb.0.as_str(), &command.object, &command.indirect) {
                // Infer only possible indirect object
                ("light", Some(object), None) if object == &self.punk => vec![{
                    Action {
                        indirect: Some(("with".into(), (&self.tinderbox).into())),
                        ..command.clone()
                    }
                }],
                // Infer only possible indirect object
                ("light", Some(object), None)
                    if object == &self.lamp && command.actor.can_see(&self.punk) =>
                {
                    vec![{
                        Action {
                            indirect: Some(("with".into(), (&self.punk).into())),
                            ..command.clone()
                        }
                    }]
                }
                // Get the punk if it is still in the box, then try again
                ("light", Some(object), Some((_, indirect)))
                    if object == &self.punk
                        && indirect == &self.tinderbox
                        && self.punk.parent().unwrap() != command.actor =>
                {
                    // These need to be in REVERSE ORDER!  TODO: fix this?
                    vec![
                        command.clone(),
                        Action {
                            actor: command.actor.clone(),
                            verb: "take".into(),
                            object: Some((&self.punk).into()),
                            text: Some("I need to be holding it to light it".to_string()),
                            ..Default::default()
                        },
                    ]
                }
                // Special case for lighting the punk since the normal
                // Ignitable rules don't apply here
                ("light", Some(object), Some((_, indirect)))
                    if object == &self.punk && indirect == &self.tinderbox =>
                {
                    vec![{
                        Action {
                            verb: "start".into(),
                            ..command.clone()
                        }
                    }]
                }
                _ => vec![],
            }
        }
        fn veto(&self, dm: &DM<'a>, command: &Action<'a>) -> Option<String> {
            match (command.verb.0.as_str(), &command.object, &command.indirect) {
                // Only works with tinderbox
                ("start", _, Some((_, indirect))) if indirect != &self.tinderbox => {
                    Some("You can only light the punk with your tinderbox.".into())
                }
                // Tinderbox must be in the room, not in inventory
                ("start", _, Some((_, indirect)))
                    if indirect.parent() != command.actor.parent() =>
                {
                    Some(
                        "You don't have enough hands!  The tinderbox needs to \
                         be set on the floor.\n  You remember a story your \
                         father told you about setting down his tinderbox in \
                         the dark and then being unable to find it.  He \
                         spent the entire night in that 30' by 30' chamber \
                         with 10' passages in the centre of the north and \
                         east walls."
                            .into(),
                    )
                }
                ("extinguish", Some(object), None)
                    if object == &self.lamp && object.has_status("aflame") =>
                {
                    dm.msg(
                        "\n  Oh!  Really?  Allow one to consult the Zarf \
                         table...  Apparently one must let you do it.",
                    );
                    None
                }
                _ => None,
            }
        }
        fn frobnicate(&self, dm: &DM<'a>, command: &Action<'a>) {
            match (command.verb.0.as_str(), &command.object, &command.indirect) {
                // Light the punk, giving PC access to FIRE.  It
                // should only be possible to get the "start" verb
                // with the punk in this context.
                ("start", Some(object), _) if !object.has_status("aflame") => {
                    // Set desc, article
                    object.data(|node| {
                        node.desc.set_short("burning punk");
                        node.desc.set_article("a");
                    });
                    object.set_status("aflame");
                    dm.msg("\n  [ The Dungeon Master secretly rolls some dice. ]  You succeed.");
                    dm.tick();
                }
                ("light", Some(object), Some((_, indirect)))
                    if object == &self.lamp && indirect == &self.punk =>
                {
                    self.lamp.borrow_mut().desc.set_long("It is lit.");
                }
                ("extinguish", Some(object), _) if object == &self.lamp => {
                    self.lamp.borrow_mut().desc.set_long("It is unlit.");
                }
                ("open", Some(object), None) if object == &self.tinderbox => {
                    dm.msg("  A puff of wood dust comes out.");
                    open_tinderbox(&self.tinderbox);
                }
                ("close", Some(object), None) if object == &self.tinderbox => {
                    dm.msg("  \"Clunk.\"");
                    close_tinderbox(&self.tinderbox);
                }
                _ => {}
            }
        }
        fn react(&self, dm: &DM<'a>, command: &Action<'a>) -> Vec<Action<'a>> {
            match (command.verb.0.as_str(), &command.object, &command.indirect) {
                // If they do anything to anything with the punk,
                // force the player to drop it, which recycles it back
                // into the box as an unlit punk
                (_, Some(_), Some((_, indirect)))
                    if indirect == &self.punk && indirect.has_status("aflame") =>
                {
                    dm.msg("  The punk burns down to your fingers.  Ouch!");
                    vec![Action {
                        actor: command.actor.clone(),
                        verb: "drop".into(),
                        object: Some((&self.punk).into()),
                        ..Default::default()
                    }]
                }
                // Reset the punk when dropped
                ("drop", Some(object), _) if object == &self.punk => {
                    self.punk.set_parent(&self.tinderbox);
                    dm.msg(format!(
                        "  {} disintegrates into {}.",
                        self.punk.the().capitalize(),
                        if self.punk.has_status("aflame") {
                            "ash"
                        } else {
                            "dust"
                        },
                    ));
                    self.punk.data(|node| {
                        node.desc.set_short("unlit punk");
                        node.desc.set_article("an");
                    });
                    vec![]
                }
                _ => vec![],
            }
        }
    }

    close_tinderbox(&tinderbox);

    dm.push_rule(Box::new(LampRule {
        lamp,
        punk,
        tinderbox,
    }));

    // Handle:
    //  - ask dm about TOPIC
    //  - ask about TOPIC
    //  - query TOPIC
    struct QueryDM;
    impl<'a> Rule<'a> for QueryDM {
        fn verbs(&self) -> &[&[&str]] {
            &[
                &[
                    // TODO: Can possibly use prepositions instead of 'about', 'for'?
                    "ask DM about",
                    "ask DM",
                    "ask about",
                    "query DM about",
                    "query DM",
                    "query about",
                    "inquire about",
                ],
                &["help me", "help"],
            ]
        }
        fn refine(&self, _: &DM<'a>, action: &Action<'a>) -> Vec<Action<'a>> {
            match action {
                // Catch general plea for help
                Action { verb, object, .. }
                    if verb == &"help me"
                        && (object.is_none() || object.as_ref().unwrap().is_pc()) =>
                {
                    vec![Action {
                        verb: "ask DM about".into(),
                        text: Some("help".into()),
                        ..Default::default()
                    }]
                }
                // Turn garbage into text so it gets past the validity
                // checks and displays
                Action { verb, garbage, .. } if verb == &"ask DM about" && garbage.is_some() => {
                    vec![Action {
                        text: garbage.clone(),
                        garbage: None,
                        ..action.clone()
                    }]
                }
                // If the player asks about an object that is present,
                // just examine it
                Action { verb, object, .. } if verb == &"ask DM about" && object.is_some() => {
                    vec![Action {
                        verb: "examine".into(),
                        ..action.clone()
                    }]
                }
                _ => vec![],
            }
        }
        fn frobnicate(&self, dm: &DM<'a>, action: &Action<'a>) {
            match action {
                Action {
                    verb,
                    text: Some(text),
                    ..
                } if verb == &"ask DM about" => {
                    dm.nl();
                    dm.msg(match text.as_str() {
                        "rpg" | "rpgs" | "role playing game" | "role playing games" | "game"
                        | "games" | "role playing" | "how to play" | "help" | "hint" => {
                            "  In Role Playing Games, players each take on \
                             the roles of a characters called the Player \
                             Characers, or PCs for short.  Another player \
                             becomes the Dungeon Master, or DM; a sort of \
                             referree who describes everything that \
                             transpires to and around the PCs.  The \
                             players say what the PCs are going to do, and \
                             using imagination and random dice, the DM tells \
                             the players what happened.  The players and \
                             their DM collaborate to tell a story where no \
                             one knows exactly what is going to happen.  \
                             This is a game bounded only by the players' \
                             collective imagination!\
                             \n  Your DM, oneself, uses advanced artificial \
                             intelligence to understand what you would like \
                             your character to do.  Type simple imperative \
                             sentences like, \"open the window,\" and one \
                             will tell you the outcome!"
                        }
                        _ => "One doesn't know anything about that.",
                    });
                }
                _ => (),
            }
        }
    }
    dm.push_rule(Box::new(QueryDM));
}

pub async fn sketch() {
    let mut dm = DM::new(0);
    init(&mut dm);
    // get the character stats
    dm.nl();
    dm.msg("  There is a goblin here and he's not happy about your trespass.");
    loop {
        dm.nl();
        dm.msg("  The goblin swings a sword.  You dodge.\n  You swing.");
        if roll!(dm, d20) > 11 {
            dm.msg(
                "  You hit!  The goblin hollers and escapes into the \
		 darkness.",
            );
            break;
        }
        dm.msg("  You miss.");
    }
    // We're just going to run the RPG stuff on this copy of the
    // character sheet for now.  Normally it wouldn't be mut.
    let mut pc_stats = dm.player().character_stats().unwrap();
    {
        let hp = pc_stats.health.hp;
        let hits = pc_stats.health.damage;
        let cur_hp = pc_stats.health.current();
        let con = pc_stats.con;
        dm.msg(format!(
            "\n  You pause to assess yourself.  You have a maximum of {hp} \
	     hitpoints.  Since you have taken {hits} damage, you have {cur_hp} \
	     hitpoints remaining.\n  Your constitution Ability Score is {con}."
        ));
    }
    let mut snake = MonsterStats::default();
    snake.health.hp = 3;
    dm.msg(format!(
        "\n  You follow into the darkness.  You come upon something.\
	 \n  Ahh, it's a snake, it's a snaaaaaaaake!  And it appears to have \
	 {} hitpoints.\
	 \n  You freak out and attack.  Not cool.",
        snake.health.hp
    ));
    for i in 0..2 {
        if roll!(dm, d20) > 10 {
            snake.health.damage += 1;
            dm.msg("  You hit and do one damage point.");
        } else {
            dm.msg("  You miss.");
        }
        dm.msg(
            "\n  You get bitten by a giant snake.  \
	     You take one point of damage.",
        );
        pc_stats.health.damage += 1;
        dm.msg("  The snake attempts to inject you with venom.");
        if roll!(dm, d20) > 11 {
            dm.msg("  You rip free before it can.");
        } else {
            dm.msg(
                "  The venom burns in your blood!  You take two more \
		 points of damage.",
            );
            pc_stats.health.damage += 2;
        }
        if i == 0 {
            dm.msg("\n  You lunge at the snake.");
        } else {
            dm.nl();
        }
    }
    loop {
        dm.nl();
        dm.msg("  You swing at the snake.");
        if roll!(dm, d20) > 10 {
            snake.health.damage += 1;
            dm.msg("  You hit and do one damage point.");
        } else {
            dm.msg("  You miss.");
        }
        if !snake.health.is_alive() {
            dm.msg("  The snake dies!");
            break;
        }
        dm.msg("  The snake strikes but misses.");
    }
    {
        let hits = pc_stats.health.damage;
        let hp = pc_stats.health.hp;
        dm.msg(format!(
            "\n  Your first innocent victim lies at your feet.  You are \
	     wounded but you will survive.  You have taken {hits} damage \
	     points out of {hp} hitpoints.\
	     \n  You stuff your pockets with a bunch of loot and continue.\
	     \n  You stumble into someone who can help you.  You gladly accept \
	     some of their limited amount of healing magic without ever \
	     questioning why.  She heals you of {hits} damage points."
        ));
    }
    pc_stats.health.damage = 0;
    while pc_stats.health.is_alive() {
        pc_stats.health.damage += 1;
        dm.msg(format!(
            "\n  Oops, you stubbed your toe.  You have {} remaining hitpoints.",
            pc_stats.health.current()
        ));
    }
    dm.msg("\n  You died!  Darn.");
    dm.msg_flush().await;
}

pub async fn intro(dm: &DM<'_>) {
    use crate::BAUD;
    dm.msg(format!("\nCONNECT {BAUD}\n"));
    dm.msg_flush().await;
    futures_timer::Delay::new(Duration::from_millis(300)).await;
    dm.msg("Please press [RETURN]: ");
    dm.msg_flush().await;
    loop {
        if let Ok(key) = dm.ui.read_key().await {
            if key == b'\r' {
                break;
            };
        };
    }
    dm.msg(include_str!("text/connect"));
    'menu: loop {
        dm.msg(include_str!("text/menu"));
        loop {
            dm.msg_flush().await;
            if let Ok(key) = dm.ui.read_key().await {
                match key {
                    b'0' => {
                        dm.msg("Introduction\n");
                        dm.msg(include_str!("text/intro"));
                        continue 'menu;
                    }
                    b'1' => {
                        dm.msg("Module B01\n\n");
                        break 'menu;
                    }
                    _ => continue,
                }
            }
        }
    }
}
