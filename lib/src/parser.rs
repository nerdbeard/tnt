use std::{collections::HashMap, fmt::Display};

use crate::{
    action::{Adverb, Preposition, Pronoun, Rule, Verb},
    scene::Node,
};

#[derive(Clone, Debug, PartialEq)]
pub enum Atom<'a> {
    Verb(Verb),
    Noun(Node<'a>),
    Adverb(Adverb),
    Predicate(Preposition),
    Garbage(Vec<Token>),
    Pronoun(Pronoun),
    // Ambiguous(&'a [Atom<'a>]),
}

pub trait Parser<'a> {
    fn parse(&self, tokens: &[Token]) -> AtomString<'a>;
}

pub trait AtomParser<'a> {
    // Match as many tokens from start as possible (unless Garbage;
    // then match zero or one, maaaybe more in special cases),
    // returning the Atom and number of tokens consumed
    fn atom(&self, tokens: &[Token]) -> (Atom<'a>, usize);
}

trait AtomMapper<'a> {
    fn atom_map(&self) -> HashMap<Vec<Token>, Atom<'a>>;
}

pub struct Input(pub String);
impl Input {
    pub fn sentences(&self) -> impl Iterator<Item = Sentence> + '_ {
        self.0.split_terminator('.').map(Sentence::from)
    }
}

impl<T: Display> From<T> for Input {
    fn from(value: T) -> Self {
        Self(value.to_string())
    }
}

#[derive(Hash, Eq, Debug, PartialEq)]
pub struct Sentence(String);
impl Sentence {
    fn words(&self) -> impl Iterator<Item = String> + '_ {
        self.0.split_whitespace().map(String::from)
    }
    pub fn tokens(&self) -> Vec<Token> {
        self.words().map(Token::from).collect()
    }
}

impl<T: Display> From<T> for Sentence {
    fn from(value: T) -> Self {
        Sentence(value.to_string())
    }
}

#[derive(PartialEq, Hash, Eq, Clone, Debug)]
pub struct Token(pub String);

impl<'a> From<&'a Token> for &'a str {
    fn from(val: &'a Token) -> Self {
        val.0.as_str()
    }
}

impl PartialEq<Token> for &str {
    fn eq(&self, other: &Token) -> bool {
        *self == other.as_str()
    }
}
impl<'a> FromIterator<&'a Token> for Vec<&'a str> {
    fn from_iter<T: IntoIterator<Item = &'a Token>>(iter: T) -> Self {
        let mut v = Vec::new();
        for token in iter {
            v.push(token.into())
        }
        v
    }
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(self.0.as_str())
    }
}

impl std::ops::Deref for Token {
    type Target = String;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl From<&str> for Token {
    fn from(value: &str) -> Self {
        Self::from(String::from(value))
    }
}

impl From<String> for Token {
    fn from(mut value: String) -> Self {
        value.make_ascii_lowercase();
        Token(value)
    }
}

impl<'a, T: AtomMapper<'a>> AtomParser<'a> for T {
    fn atom(&self, tokens: &[Token]) -> (Atom<'a>, usize) {
        let atom_map = self.atom_map();
        for width in (1..=tokens.len()).rev() {
            if let Some(atom) = atom_map.get(&Vec::from(&tokens[..width])) {
                return (atom.clone(), width);
            }
        }
        (Atom::Garbage(vec![]), 0)
    }
}

impl<'a> AtomParser<'a> for Vec<Box<dyn AtomParser<'a>>> {
    fn atom(&self, tokens: &[Token]) -> (Atom<'a>, usize) {
        let mut best: (Atom, usize) = (Atom::Garbage(vec![]), 0);
        for parser in self {
            let challenger = parser.atom(tokens);
            if challenger.1 > best.1 {
                best = challenger;
            }
        }
        best
    }
}

/// The hard coded parser rules
pub struct CoreParser;

impl<'a> AtomParser<'a> for CoreParser {
    fn atom(&self, tokens: &[Token]) -> (Atom<'a>, usize) {
        if let Some(token) = tokens.get(0) {
            if token.0.ends_with("ly") {
                (Atom::Adverb(Adverb::from(token.clone())), 1)
            } else {
                match token.as_str() {
                    // Maybe hardcode some adverbs that don't end in -ly
                    "at" | "in" | "on" | "with" => {
                        (Atom::Predicate(Preposition::from(token.clone())), 1)
                    }
                    "it" | "him" | "her" | "them" => {
                        (Atom::Pronoun(Pronoun::from(token.clone())), 1)
                    }
                    _ => (Atom::Garbage(vec![]), 0),
                }
            }
        } else {
            (Atom::Garbage(vec![]), 0)
        }
    }
}

impl<'a> AtomMapper<'a> for &[Box<dyn AtomMapper<'a>>] {
    fn atom_map(&self) -> HashMap<Vec<Token>, Atom<'a>> {
        let mut map = HashMap::new();
        for mapper in self.iter() {
            for (k, v) in mapper.atom_map().into_iter() {
                map.insert(k, v);
            }
        }
        map
    }
}

impl<'a> AtomMapper<'a> for Node<'a> {
    /// Match nouns against both [self.borrow().desc.name] and
    /// [self.borrow().names].
    /// Match verbs against [self.verbs()]
    fn atom_map(&self) -> HashMap<Vec<Token>, Atom<'a>> {
        let mut map = HashMap::new();
        // Identify all the phrases that could refer to this node as a noun
        for noun in self.nouns() {
            map.insert(noun, Atom::Noun(self.clone()));
        }
        // Identify all the phrases that this node exports as a special verb
        for verb in self.verbs() {
            map.insert(verb.0, Atom::Verb(verb.1));
        }
        map
    }
}

impl<'a> AtomMapper<'a> for Box<dyn Rule<'a>> {
    fn atom_map(&self) -> HashMap<Vec<Token>, Atom<'a>> {
        let mut verbs: HashMap<Vec<Token>, Atom<'a>> = Default::default();
        for phrases in self.verbs() {
            let mut phrases = phrases.iter();
            let canonical: &str = phrases.next().expect("Blank verb table entry");
            let verb = Atom::Verb(canonical.into());
            for phrase in phrases {
                let phrase = phrase.split_whitespace().map(Token::from).collect();
                verbs.insert(phrase, verb.clone());
            }
            // Add the canonical form itself
            verbs.insert(
                canonical.split_whitespace().map(Token::from).collect(),
                verb,
            );
        }
        verbs
    }
}

impl<'a> Node<'a> {
    fn nouns(&self) -> Vec<Vec<Token>> {
        let mut names: Vec<Vec<_>> = vec![];
        let name = Sentence::from(self.borrow().desc.name()).tokens();
        if !name.is_empty() {
            names.push(name);
        }
        for name in self.borrow().names.iter() {
            names.push(Sentence::from(name).tokens())
        }
        names
    }
    fn verbs(&self) -> HashMap<Vec<Token>, Verb> {
        Default::default()
    }
}

impl<'a> Parser<'a> for &[Box<&dyn AtomParser<'a>>] {
    fn parse(&self, mut tokens: &[Token]) -> AtomString<'a> {
        let mut atoms = vec![];
        while !tokens.is_empty() {
            let mut result = (Atom::Garbage(vec![]), 0);
            for parser in self.iter() {
                let new_result = parser.atom(tokens);
                if new_result.1 > result.1 {
                    result = new_result;
                }
            }
            if result.1 == 0 {
                // This is garbage.  Was the last one, also?
                if let Some(Atom::Garbage(garbage)) = atoms.last() {
                    // Yes, throw this on the pile
                    let mut garbage = garbage.to_vec();
                    garbage.push(tokens[0].clone());
                    *atoms.last_mut().unwrap() = Atom::Garbage(garbage);
                } else {
                    // No, start a new pile
                    atoms.push(Atom::Garbage(Vec::from(&tokens[0..1])));
                }
                tokens = &tokens[1..];
            } else {
                // This is the good stuff.
                atoms.push(result.0);
                tokens = &tokens[result.1..];
            }
        }
        AtomString(atoms)
    }
}

impl<'a> Parser<'a> for Node<'a> {
    fn parse(&self, tokens: &[Token]) -> AtomString<'a> {
        let mut atom_parsers: Vec<Box<&dyn AtomParser>> = vec![Box::new(&CoreParser)];
        let nodes: Vec<_> = self.visible().collect();
        for node in &nodes {
            atom_parsers.push(Box::new(node));
        }
        atom_parsers.as_slice().parse(tokens)
    }
}

impl Display for Atom<'_> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let (kind, value) = match self {
            Atom::Verb(v) => ("verb", v.0 .0.to_string()),
            Atom::Adverb(v) => ("adverb", v.0 .0.to_string()),
            Atom::Noun(v) => ("noun", v.to_string()),
            Atom::Predicate(v) => ("predicate", v.0.to_string()),
            Atom::Pronoun(v) => ("pronoun", v.0.to_string()),
            Atom::Garbage(v) => (
                "garbage",
                format!(
                    "\"{}\"",
                    v.iter()
                        .map(|t| t.to_string())
                        .collect::<Vec<_>>()
                        .join(" ")
                ),
            ),
        };
        f.write_fmt(format_args!("{kind}:{value}"))
    }
}

#[derive(Debug, PartialEq)]
pub struct AtomString<'a>(pub Vec<Atom<'a>>);

impl<'a> From<&[Atom<'a>]> for AtomString<'a> {
    fn from(value: &[Atom<'a>]) -> Self {
        AtomString(value.to_vec())
    }
}

impl<'a> From<Vec<Atom<'a>>> for AtomString<'a> {
    fn from(value: Vec<Atom<'a>>) -> Self {
        AtomString(value)
    }
}

impl<'a> Display for AtomString<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut i = self.0.iter();
        i.next().map(|atom| f.write_fmt(format_args!("{atom}")));
        for atom in i {
            f.write_fmt(format_args!(" {atom}"))?;
        }
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn hardcoded_parsers() {
        let atom_parsers = vec![Box::new(&CoreParser as &dyn AtomParser)];
        let parser = &atom_parsers.as_slice() as &dyn Parser;
        assert_eq!(
            parser.parse(&[Token::from("token")]),
            vec![Atom::Garbage(vec![Token::from("tokeN")])].into()
        );
        assert_eq!(
            parser.parse(&[Token::from("magicly")]),
            vec![Atom::Adverb(Adverb::from("magIcly"))].into()
        );
        assert_eq!(
            parser.parse(&[Token::from("it")]),
            vec![Atom::Pronoun(Pronoun::from("IT"))].into()
        );
        assert_eq!(
            parser.parse(&[Token::from("pet"), Token::from("it")]),
            vec![
                Atom::Garbage(vec![Token::from("pet")]),
                Atom::Pronoun(Pronoun::from("IT"))
            ]
            .into()
        );
    }
    #[test]
    fn node_parser() {
        let node = Node::default();
        node.borrow_mut().desc.set_short("node");
        let atom_parsers = vec![Box::new(&node as &dyn AtomParser)];
        let parser = &atom_parsers.as_slice() as &dyn Parser;
        assert_eq!(
            parser.parse(&[Token::from("node")]),
            vec![Atom::Noun(node)].into()
        );
    }
    #[test]
    fn node_mapper() {
        let node = Node::default();
        let mapper = &node as &dyn AtomMapper;
        node.borrow_mut().desc.set_short("node");
        let map = mapper.atom_map();
        let mut keys = map.keys();
        let want = vec![Token::from("node")];
        assert_eq!(keys.next(), Some(&want));
        assert_eq!(keys.next(), None);
    }
    #[test]
    fn node_atom_parser() {
        let node = Node::default();
        let parser = &node as &dyn AtomParser;
        node.borrow_mut().desc.set_short("node");
        assert_eq!(
            parser.atom(&[Token::from("node")]),
            (Atom::Noun(node.clone()), 1)
        );
    }
}
