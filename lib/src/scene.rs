//! A Scene is a tree of NodeData structs.  It doesn't have its own type anymore.

use std::{
    cell::RefCell,
    collections::{hash_map::DefaultHasher, HashMap, HashSet},
    fmt::{Debug, Display},
    hash::{Hash, Hasher},
    ops::Deref,
    ptr::hash,
    rc::Rc,
};

use crate::{
    character::Character, desc::Description, dm::DM, parser::Token, stats::CharacterStats,
    string::Munge,
};

/// The interior data of a node in the scene graph.
// There are reference cycles that should be replaced by WeakRefs.  Oh
// well, we'll just leak memory for now, probably until loading and
// saving is implemented.
#[derive(Default)]
pub struct NodeData<'a> {
    // Should be the WeakRef.
    parent: Option<Node<'a>>,
    // Children should be owned by the parent but everybody owns
    // everybody right now.
    children: NodeSet<'a>,
    /// Synonyms used by the parser to let the player refer to the
    /// node as a noun.
    pub names: HashSet<&'a str>,
    /// Free-form statuses.  Some are used by the system and core
    /// library, others are defined and used only by the adventure.
    // TODO: Document
    pub statuses: HashSet<&'a str>,
    /// Name, article, short and long descriptions, maybe more.
    // TODO: Why is this composed?  What else needs a description?
    pub desc: Description<'a>,
    /// Character stats if applicable.  A character gets to generate
    /// Actions.
    pub character: Option<Character>,
    /// Eg, other end of an exit object.  Free form node ref.
    pub target: Option<Node<'a>>,
    /// General purpose variable, reserved for adventure use.
    pub general: i16,
}

/// Provides a HashSet-like interface for [[Node]]s which can't be
/// stuffed directly into a HashSet.
#[derive(Default, PartialEq, Clone)]
pub struct NodeSet<'a>(HashMap<u64, Node<'a>>);

impl<'a> NodeSet<'a> {
    fn key(node: &Node<'a>) -> u64 {
        let mut h = DefaultHasher::default();
        hash(&*node.borrow(), &mut h);
        h.finish()
    }
    pub fn contains(&self, node: &Node<'a>) -> bool {
        self.0.contains_key(&NodeSet::key(node))
    }
    pub fn insert(&mut self, node: Node<'a>) -> Option<Node<'a>> {
        self.0.insert(NodeSet::key(&node), node)
    }
    pub fn remove(&mut self, node: &Node<'a>) -> Option<Node<'a>> {
        self.0.remove(&NodeSet::key(node))
    }
    pub fn nodes(&self) -> impl Iterator<Item = &Node<'a>> {
        self.0.values()
    }
}

impl<'a> NodeData<'a> {
    /// Initialize using a constructor function
    pub fn new(ctor: impl Fn(&mut NodeData<'a>)) -> NodeData<'a> {
        let mut node = NodeData::default();
        ctor(&mut node);
        node
    }
    pub fn children(&self) -> NodeSet<'a> {
        self.children.clone()
    }
    /// Count how many words can be matched from the beginning of the
    /// given phrase.  Match against both self.desc.name and
    /// self.names.
    pub fn match_name(&self, words: &[&str]) -> usize {
        // Identify all the phrases that could refer to this node
        let mut names: Vec<Vec<Token>> = vec![];
        {
            let name = self.desc.name().tokenize();
            if !name.is_empty() {
                names.push(name);
            }
            for name in self.names.iter() {
                names.push(name.tokenize())
            }
        }
        // Check for matches using the largest window we can, and
        // reduce it until we get a hit or it reaches zero.
        let mut window_size = names
            .iter()
            .map(|v| v.len())
            .reduce(usize::max)
            .unwrap_or(0)
            .min(words.len());
        while window_size > 0 {
            for name in &names {
                if words[..window_size] == name[..window_size.min(name.len())] {
                    return window_size;
                }
            }
            window_size -= 1;
        }
        0
    }
}

impl<'a> Debug for NodeData<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str("NodeData")?;
        f.debug_map()
            .entry(&"parent", &self.parent.clone().map(|n| format!("{n}")))
            .entry(
                &"children",
                &self.children.nodes().collect::<Vec<_>>().list_nodes(),
            )
            .entry(&"desc", &self.desc)
            .entry(&"character", &self.character)
            .entry(&"target", &self.target)
            .finish()
    }
}

impl<'a> Hash for NodeData<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.parent.hash(state);
        self.desc.hash(state);
        self.character.hash(state);
        self.target.hash(state);
    }
}

/// Two NodesData structs are equal only if they have the same address
impl<'a> PartialEq for NodeData<'a> {
    fn eq(&self, other: &Self) -> bool {
        std::ptr::eq(self, other)
    }
}

/// Might have been called NodeRef because it's just a cloneable
/// reference to a NodeData.
#[derive(Default, Clone)]
pub struct Node<'a>(Rc<RefCell<NodeData<'a>>>);

/// Writes the name with article.
impl<'a> Display for Node<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.write_str(&self.the())
    }
}

/// Derefs to its guts so you can call .borrow() etc on it.
impl<'a> Deref for Node<'a> {
    type Target = Rc<RefCell<NodeData<'a>>>;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

/// Two Nodes are equal if their NodeData structures have the same
/// address (checked in its own PartialEq)
impl<'a> PartialEq for Node<'a> {
    fn eq(&self, other: &Self) -> bool {
        *self.borrow() == *other.borrow()
    }
}

impl<'a> Debug for Node<'a> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_tuple("Node").field(&self.borrow()).finish()
    }
}

/// Use the inner HashData struct's address as the hash value
impl<'a> Hash for Node<'a> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        state.write_usize(&*self.borrow() as *const _ as usize);
    }
}

/// Convenienly wrap a NodeData in a new Node reference
impl<'a> From<NodeData<'a>> for Node<'a> {
    fn from(value: NodeData<'a>) -> Self {
        Node(Rc::new(RefCell::new(value)))
    }
}

impl<'a> Node<'a> {
    /// Mutate the interior NodeData.
    pub fn data(&self, func: impl Fn(&mut NodeData<'a>)) {
        func(&mut self.borrow_mut())
    }
    /// Create a new node, set its parent, call the constructor, and
    /// return it.
    pub fn new_child(&mut self, ctor: impl Fn(&mut NodeData<'a>)) -> Node<'a> {
        let node = Node::from(NodeData::default());
        node.set_parent(self);
        node.data(ctor);
        node
    }
    /// Create a new_child with no constructor.
    pub fn default_child(&mut self) -> Node<'a> {
        self.new_child(|_| ())
    }
    /// Graph is expected to be acyclic but nothing guarantees that so
    /// be careful when updating the tree.
    pub fn set_parent(&self, parent: &Node<'a>) {
        if let Some(old_parent) = &self.borrow().parent {
            old_parent.borrow_mut().children.remove(self);
        }
        parent.borrow_mut().children.insert(self.clone());
        self.borrow_mut().parent = Some(parent.clone());
    }
    /// Parent is None for scene root.
    pub fn parent(&self) -> Option<Node<'a>> {
        self.borrow().parent.clone()
    }
    pub fn children(&self) -> Vec<Node<'a>> {
        self.borrow().children.nodes().cloned().collect()
    }
    pub fn has_status(&self, status: &str) -> bool {
        self.borrow().statuses.contains(status)
    }
    pub fn set_status(&self, status: &'a str) -> bool {
        self.borrow_mut().statuses.insert(status)
    }
    pub fn clear_status(&self, status: &str) -> bool {
        self.borrow_mut().statuses.remove(status)
    }
    /// Prose describing how the beholder perceives a node (if
    /// applicable).  May be an empty string.
    pub fn describe(&self, beholder: &Node) -> String {
        let mut desc = vec![];
        self.describe_inner(beholder, &mut desc);
        desc.join("\n  ")
    }
    fn describe_inner(&self, beholder: &Node, desc: &mut Vec<String>) {
        let mut data = self.borrow_mut();
        if !data.statuses.contains("invisible") {
            desc.push(data.desc.describe(beholder));
        }
        if data.statuses.contains("transparent")
            && (data.statuses.contains("open") || !data.statuses.contains("openable"))
        {
            drop(data);
            self.describe_children(beholder, desc);
        }
    }
    fn describe_children(&self, beholder: &Node, desc: &mut Vec<String>) {
        let data = self.borrow();
        let mut long = vec![];
        let mut short = vec![];
        for item in data.children.nodes() {
            let data = item.borrow();
            if data.statuses.contains("invisible") {
                continue;
            }
            if data.desc.brief() {
                &mut short
            } else {
                &mut long
            }
            .push(item);
        }
        for item in long {
            item.describe_inner(beholder, desc);
        }
        if !short.is_empty() {
            let short = short.list_nodes();
            if !short.is_empty() {
                desc.push(format!("You also see {short}."));
            }
        }
    }
    /// The common element of PCs and NPCs.
    pub fn character_stats(&self) -> Option<CharacterStats> {
        let data = self.borrow();
        match &data.character {
            Some(Character::Pc { stats }) => Some(*stats),
            Some(Character::Npc { stats }) => Some(*stats),
            _ => None,
        }
    }
    /// This node and every child recursively.
    pub fn walk(&self) -> NodeIterator<'a> {
        NodeIterator::from(self)
    }
    /// walk() without this node.
    pub fn descendants(&self) -> NodeIterator<'a> {
        // Just throw away the first one!
        let mut i = self.walk();
        i.next();
        i
    }
    /// Child nodes reachable via visibility rules.
    pub fn visible(&self) -> VisibleNodeIterator<'a> {
        VisibleNodeIterator::from(self)
    }
    /// Child nodes that have Some in their character field.
    pub fn characters(&self) -> CharacterNodeIterator<'a> {
        CharacterNodeIterator::from(self)
    }
    /// Test visiblity between two nodes.
    // TODO: Shouldn't this be going via parent()?  But presumably it
    // works?
    pub fn can_see(&self, other: &Node<'a>) -> bool {
        self.visible().any(|node| &node == other)
    }
    /// Name with article, unless it's the PC, then it's "you".
    pub fn the(&self) -> String {
        if self.is_pc() {
            "you".into()
        } else {
            self.borrow().desc.the()
        }
    }
    /// Used by library to keep track of what objects have been examined.
    // Why are examine and describe different functions?  What's going
    // on here?
    pub fn examine(&self) -> String {
        let mut text = vec![self.borrow().desc.examine()];
        if self.has_status("transparent")
            && ((!self.has_status("openable")) || self.has_status("open"))
        {
            self.describe_children(self, &mut text);
        }
        self.set_status("examined");
        text.join("\n  ")
    }
    /// Announce the message to the PC if they are a descendant of
    /// this node.
    pub fn tell(&self, dm: &DM, message: &str) {
        for node in self.walk() {
            if node.is_pc() {
                dm.msg(message);
                break;
            }
        }
    }

    pub fn is_pc(&self) -> bool {
        matches!(self.borrow().character, Some(Character::Pc { .. }))
    }

    pub fn is_npc(&self) -> bool {
        matches!(self.borrow().character, Some(Character::Npc { .. }))
    }
}

/// Operations you might want to perform on a colletion of Nodes.
pub trait Nodes {
    fn list_nodes(&self) -> String;
}

/// A NodeSet as a collection of Nodes.
impl<'a> Nodes for NodeSet<'a> {
    fn list_nodes(&self) -> String {
        self.nodes().collect::<Vec<_>>().list_nodes()
    }
}

/// An array as a collection of Nodes.
impl<'a> Nodes for [&Node<'a>] {
    fn list_nodes(&self) -> String {
        if let Some(node0) = self.get(0).map(|n| n.the()) {
            if self.len() == 1 {
                node0
            } else {
                format!(
                    "{}, and {}",
                    self[1..]
                        .iter()
                        // Why doesn't this work?
                        // .map(Node::the)
                        .map(|node| node.the())
                        .collect::<Vec<_>>()
                        .join(", "),
                    node0
                )
            }
        } else {
            String::from("nothing")
        }
    }
}

/// Implements Node::walk.
pub struct NodeIterator<'a>(Vec<Node<'a>>);
impl<'a> From<&Node<'a>> for NodeIterator<'a> {
    fn from(value: &Node<'a>) -> Self {
        Self(vec![value.clone()])
    }
}

impl<'a> Iterator for NodeIterator<'a> {
    type Item = Node<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop().map(|node| {
            self.0
                .extend(node.borrow().children.nodes().map(Node::clone));
            node
        })
    }
}

/// Implements Node::visible.
pub struct VisibleNodeIterator<'a>(Vec<Node<'a>>);
impl<'a> From<&Node<'a>> for VisibleNodeIterator<'a> {
    fn from(value: &Node<'a>) -> Self {
        Self(vec![value.clone()])
    }
}

impl<'a> Iterator for VisibleNodeIterator<'a> {
    type Item = Node<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        self.0.pop().map(|node| {
            if node.has_status("transparent")
                && ((!node.has_status("openable")) || node.has_status("open"))
            {
                for child in node.children() {
                    // if !child.has_status("invisible") {}
                    self.0.push(child);
                }
            }
            node
        })
    }
}

/// Implements Node::characters.
pub struct CharacterNodeIterator<'a>(Vec<Node<'a>>);
impl<'a> From<&Node<'a>> for CharacterNodeIterator<'a> {
    fn from(value: &Node<'a>) -> Self {
        Self(vec![value.clone()])
    }
}

impl<'a> Iterator for CharacterNodeIterator<'a> {
    type Item = Node<'a>;
    fn next(&mut self) -> Option<Self::Item> {
        while let Some(node) = self.0.pop() {
            for child in node.children() {
                self.0.push(child);
            }
            if node.borrow().character.is_some() {
                return Some(node);
            };
        }
        None
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn node() {
        let mut s = Node::default();
        let n1 = s.default_child();
        assert_eq!(n1, n1.clone());
        let n2 = s.default_child();
        assert_ne!(n1, n2);
        n1.set_parent(&n2);
        assert!(
            n2.borrow().children.contains(&n1),
            "{n1:?} wasn't in {n2:?}.children == {:?}",
            n2.borrow().children.nodes().collect::<Vec<_>>(),
        );
        assert_eq!(n1.parent(), Some(n2));
    }
    #[test]
    fn describe() {
        let node1 = Node::default();
        assert_eq!(&node1.describe(&node1), "");
        let node2 = Node::default();
        node1.borrow_mut().desc.set_description("desc1");
        node2.borrow_mut().desc.set_description("desc2");
        node1.set_parent(&node2);
        assert_eq!(&node1.describe(&node1), "desc1");
        assert_eq!(&node2.describe(&node2), "desc2");
        node2.set_status("transparent");
        assert_eq!(&node2.describe(&node2), "desc2\n  desc1");
    }
}
