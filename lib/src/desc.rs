use crate::scene::Node;

/// Doesn't care about beholder.  Displays an initial description the
/// first time it is available, and the normal description if
/// available every other time; in either case, followed by a newline.
/// If neither is set, renders an empty string (with no newline).  A
/// new "initial" description may be set after the first one has
/// fired, and it will also be displayed once at the next opportunity.
/// Setting the description or initial to an empty string actually unsets it.
#[derive(Debug, Default, PartialEq, Eq, Hash)]
pub struct Description<'a> {
    initial: Option<&'a str>,
    description: Option<&'a str>,
    short: Option<&'a str>,
    article: Option<&'a str>,
    long: Option<&'a str>,
}

macro_rules! setter {
    ($func_name:ident, $property:ident) => {
        pub fn $func_name(&mut self, v: &'a str) {
            self.$property = if v.is_empty() { None } else { Some(v) };
        }
    };
}
impl<'a> Description<'a> {
    setter!(set_initial, initial);
    setter!(set_description, description);
    setter!(set_short, short);
    setter!(set_article, article);
    setter!(set_long, long);

    pub fn has_initial(&self) -> bool {
        self.initial.is_some()
    }
    pub fn describe(&mut self, _beholder: &Node) -> String {
        if let Some(initial) = self.initial.take() {
            initial
        } else if let Some(description) = self.description {
            description
        } else if let Some(short) = self.short {
            short
        } else {
            // Return directly so no newline is appended
            return String::new();
        }
        .to_string()
    }
    pub fn examine(&self) -> String {
        self.long.map(String::from).unwrap_or_else(|| {
            self.description
                .map(String::from)
                .unwrap_or_else(|| format!("It's just {}.", self.the()))
        })
    }
    pub fn name(&self) -> &str {
        self.short.unwrap_or("nameless thing")
    }
    pub fn the(&self) -> String {
        format!(
            "{}{}",
            match self.article {
                Some(article) => {
                    format!("{article} ")
                }
                None => {
                    String::new()
                }
            },
            self.name(),
        )
    }
    /// Should this item be rounded up into the brief description?
    /// Alternatively it will be given its own stanza.
    pub fn brief(&self) -> bool {
        self.short.is_some() && self.initial.is_none() && self.description.is_none()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn basic_description() {
        let mut desc = Description::default();
        let initial = "initial";
        let description = "description";
        let beholder = &Node::default();
        desc.set_description(description);
        desc.set_initial(initial);
        assert_eq!(desc.describe(beholder), initial);
        assert_eq!(desc.describe(beholder), description);
        assert_eq!(desc.describe(beholder), description);
        desc.set_initial(initial);
        assert_eq!(desc.describe(beholder), initial);
        assert_eq!(desc.describe(beholder), description);
        assert_eq!(desc.describe(beholder), description);
        desc.set_description("");
        assert_eq!(desc.describe(beholder), "");
    }
}
