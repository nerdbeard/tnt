use std::{fmt::Display, ops::Deref};

#[derive(Default, Hash, Debug, PartialEq, Eq, Copy, Clone)]
pub struct Health {
    pub hp: u32,
    pub damage: u32,
}

impl Health {
    pub fn is_alive(&self) -> bool {
        self.damage < self.hp
    }
    pub fn current(&self) -> u32 {
        if self.damage > self.hp {
            0
        } else {
            self.hp - self.damage
        }
    }
}

#[derive(Default, Hash, Debug, PartialEq, Eq, Copy, Clone)]
pub struct Ability(u32);

impl Display for Ability {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        Display::fmt(&self.0, f)
    }
}

impl Deref for Ability {
    type Target = u32;
    fn deref(&self) -> &Self::Target {
        &self.0
    }
}
impl From<u32> for Ability {
    fn from(value: u32) -> Self {
        Self(value)
    }
}

#[derive(Default, Hash, PartialEq, Eq, Debug, Copy, Clone)]
pub struct CharacterStats {
    pub str: Ability,
    pub dex: Ability,
    pub int: Ability,
    pub con: Ability,
    pub chr: Ability,
    pub wis: Ability,
    pub health: Health,
}

#[derive(Default, PartialEq, Eq, Hash, Debug, Copy, Clone)]
pub struct MonsterStats {
    pub health: Health,
}
