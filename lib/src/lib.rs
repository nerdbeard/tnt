pub mod action;
pub mod adventure;
pub mod character;
pub mod ui;
#[macro_use]
pub mod core;
mod desc;
#[macro_use]
mod dice;
pub mod dm;
mod parser;
pub mod scene;
pub mod stats;
mod string;

const BAUD: u64 = 2400;
