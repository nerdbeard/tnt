use std::sync::Arc;

use futures::lock::Mutex;
use web_sys::{HtmlElement, HtmlInputElement};
use yew::prelude::*;

use tnt::{
    dm::DM,
    ui::{ControlCode, InputSender, OutputEvent},
};

pub struct App<'a> {
    dm: Arc<Mutex<DM<'a>>>,
    input_send: InputSender,
    node: NodeRef,
    text: String,
}

pub enum Msg {
    InputReceived(tnt::ui::InputEvent),
    OutputReceived(tnt::ui::OutputEvent),
    Started,
}

impl Component for App<'static> {
    type Message = Msg;

    type Properties = ();

    fn create(ctx: &Context<Self>) -> Self {
        wasm_logger::init(wasm_logger::Config::default());
        let mut dm = DM::new(0);
        let (input_send, mut output_receive) = dm.ui.connect().unwrap();
        let dm = Arc::new(Mutex::new(dm));
        {
            let link = ctx.link().clone();
            yew::platform::spawn_local(async move {
                log::debug!("Starting output_monster");
                while let Some(event) = output_receive.recv().await {
                    link.send_message(Msg::OutputReceived(event));
                }
                log::debug!("Exiting output_monster");
            })
        };
        let node = NodeRef::default();

        Self {
            dm,
            input_send,
            node,
            text: String::new(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Msg::InputReceived(event) => {
                if let Some(input) = self.node.cast::<HtmlInputElement>() {
                    input.set_value("");
                }
                if let Err(err) = self.input_send.send(event) {
                    log::debug!("DM hung up on me: {err}");
                }
                false
            }
            Msg::OutputReceived(OutputEvent::Char(character)) => {
                self.text.push(character);
                if character == '\n' {
                    // Maintain 25 lines
                    let mut screen: Vec<_> = self.text.split('\n').rev().take(25).collect();
                    screen.reverse();
                    self.text = screen.join("\n");
                };
                true
            }
            Msg::Started => {
                let dm = self.dm.clone();
                yew::platform::spawn_local(async move {
                    log::debug!("Beginning main sequence");
                    let mut dm = dm.lock().await;
                    tnt::adventure::init(&mut dm);
                    tnt::adventure::intro(&mut dm).await;
                    gloo_dialogs::alert(&format!("{:?}", dm.game_loop().await));
                    log::debug!("Main sequence complete");
                });
                false
            }
            Msg::OutputReceived(OutputEvent::Control(ControlCode::Backspace)) => {
                if !(self.text.is_empty() || self.text.ends_with('\n')) {
                    self.text.pop();
                    true
                } else {
                    false
                }
            }
            _ => todo!(),
        }
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let link = ctx.link().clone();
        let input_cb: Callback<KeyboardEvent> =
            Callback::from(move |event: web_sys::KeyboardEvent| {
                let key_code = event.key_code();
                log::debug!("Key handler called: {key_code}");
                link.send_message(Msg::InputReceived(tnt::ui::InputEvent::Char(
                    // ha ha ok
                    key_code as u8,
                )));
            });
        html! {
            <main>
            { self.text.as_str() }
            <input ref={self.node.clone()} onkeydown={input_cb} />
            </main>
        }
    }

    fn rendered(&mut self, ctx: &Context<Self>, first_render: bool) {
        if first_render {
            // Let's send the startup message here once everything
            // else is finalized -- not sure if that's a thing or not
            ctx.link().send_message(Msg::Started);
        }

        // Try to focus the input widget
        if let Some(input) = self.node.cast::<HtmlElement>() {
            input.focus().unwrap();
        } else {
            log::debug!("Can't find input widget: {:?}", self.node);
        }
    }
}
