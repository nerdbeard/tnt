use std::{
    fmt::Debug,
    io::{stdout, Read, Write},
    os::fd::AsRawFd,
    sync::{
        atomic::{AtomicBool, Ordering::Relaxed},
        Arc,
    },
    thread::{Builder, JoinHandle},
};

use raw_tty::IntoRawMode;
use terminal_size::terminal_size;
use termios::Termios;

use tnt::ui::{ControlCode, InputEvent, InputSender, OutputEvent, OutputReceiver, UserInterface};

pub struct TermApp {
    quitting: Arc<AtomicBool>,
    kb_thread: JoinHandle<std::io::Result<()>>,
    output_thread: JoinHandle<std::io::Result<()>>,
}

impl Debug for TermApp {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("TermApp")
            .field("quitting", &self.quitting)
            .field("kb_thread", &self.kb_thread)
            .field("output_thread", &self.output_thread)
            .finish()
    }
}

impl TermApp {
    // Get the terminal width for centreing output.  Configure stdin:
    // read timeout 0.1 secs, raw, unbuffered, no echo.  Save initial
    // term config.  Start thread to report key events with shutdown
    // flag and handle.
    pub fn new(
        stdin: impl Read + AsRawFd + Send + 'static,
        ui: &mut UserInterface,
    ) -> std::io::Result<Self> {
        // Get endpoints from UI; give to threads
        let (input_sender, output_receiver) = ui.connect().unwrap();

        // Flag for signalling threads it's time to quit
        let quitting: Arc<AtomicBool> = Default::default();

        // Start keyboard thread
        let kb_thread = Self::kb_thread(quitting.clone(), stdin, input_sender).unwrap();

        // Start output thread
        let output_thread = Self::output_thread(output_receiver).unwrap();

        Ok(Self {
            quitting,
            kb_thread,
            output_thread,
        })
    }

    pub fn quit(self) {
        // Kill kb thread
        self.quitting.store(true, Relaxed);
        self.kb_thread
            .join()
            .unwrap()
            .unwrap_or_else(|err| log::error!("From keyboard thread: {err}"));
        // Output thread dies when its channel gets closed
        self.output_thread
            .join()
            .unwrap()
            .unwrap_or_else(|err| log::error!("From output thread: {err}"));
    }

    fn output_thread(
        mut output_receiver: OutputReceiver,
    ) -> Result<JoinHandle<Result<(), std::io::Error>>, std::io::Error> {
        // Get the term width, build padding string
        let padding: String = " "
            .chars()
            .cycle()
            .take(((terminal_size().map(|x| x.0 .0).unwrap_or(80) - 40) >> 1).into())
            .collect();

        Builder::new().name("output thread".into()).spawn(move || {
            while let Some(event) = output_receiver.blocking_recv() {
                match event {
                    OutputEvent::Char(c) => {
                        print!("{c}");
                        if c == '\n' || c == '\r' {
                            print!("\r{padding}");
                        }
                    }
                    OutputEvent::Control(code) => match code {
                        ControlCode::Backspace => print!(" "),
                        ControlCode::Bell => print!(""),
                        code => {
                            log::error!("Unhandled control code: {code:?}");
                            print!("?")
                        }
                    },
                };
                stdout()
                    .flush()
                    .map_err(|err| {
                        log::error!("Flushing stdout: {err}");
                    })
                    .unwrap();
            }
            Ok(())
        })
    }

    fn kb_thread(
        quitting: Arc<AtomicBool>,
        stdin: impl Read + AsRawFd + Send + 'static,
        kb_send: InputSender,
    ) -> Result<JoinHandle<Result<(), std::io::Error>>, std::io::Error> {
        Builder::new()
            .name("keyboard reader".into())
            .spawn(move || -> std::io::Result<()> {
                let stdin_fd = stdin.as_raw_fd();

                // No echo, raw input, no input buffer
                let mut stdin: Box<dyn Read> =
		    // Try to go into raw mode, but if we can't,
		    // continue anyway
                    if stdin_fd >= 0 {
                        Box::new(stdin.into_raw_mode().unwrap())
                    } else {
			log::warn!("Can't go raw");
                        Box::new(stdin)
                    };
                let mut buf: [u8; 1] = [0];
                let termios = set_read_timeout(stdin_fd);
                while !quitting.load(Relaxed) {
                    match stdin.read(&mut buf) {
                        Ok(size) => {
                            let event = match size {
                                0 => InputEvent::Idle,
                                1 => InputEvent::Char(buf[0]),
                                _ => unreachable!(),
                            };
                            if let Err(err) = kb_send.send(event) {
                                // The channel has been closed, no
                                // point in continuing
                                log::trace!("Sending from kb_thread: {err}");
                                break;
                            };
                        }
                        Err(err) => {
                            log::error!("Reading from stdin: {err}");
                        }
                    };
                }
                // Restore terminal state
                if let Ok(Some(termios)) = termios {
                    log::trace!("Restoring terminal settings");
                    termios::tcsetattr(stdin_fd, termios::TCSANOW, &termios)
                        .unwrap_or_else(|err| log::error!("Restoring termios: {err}"))
                }

                log::debug!("Exiting keyboard thread");
                Ok(())
            })
    }
}

fn set_read_timeout(fd: i32) -> Result<Option<Termios>, std::io::Error> {
    let termios: Option<Termios> = if let Ok(termios) = Termios::from_fd(fd) {
        let mut new_termios = termios;
        // Time out if zero characters received in VTIME
        new_termios.c_cc[termios::VMIN] = 0;
        // One decisecond
        new_termios.c_cc[termios::VTIME] = 1;
        termios::tcsetattr(fd, termios::TCSANOW, &new_termios)?;
        Some(termios)
    } else {
        log::warn!("Did not set termios");
        None
    };
    Ok(termios)
}

#[cfg(xctest)]
mod tests {
    use std::{cell::RefCell, io::Error};

    use tnt::ui::{input_event_stream, output_event_stream, Null};

    use super::*;
    #[tokio::test]
    async fn test() {
        #[derive(Clone)]
        struct TestStdin(Vec<u8>);
        impl AsRawFd for TestStdin {
            fn as_raw_fd(&self) -> std::os::fd::RawFd {
                -11
            }
        }
        impl Read for TestStdin {
            fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
                if let Some(c) = self.0.pop() {
                    buf[0] = c;
                    Ok(1)
                } else {
                    Err(Error::from(std::io::ErrorKind::UnexpectedEof))
                }
            }
        }
        struct TestConsumer {
            events: Arc<RefCell<Vec<InputEvent>>>,
        }
        let (input_send, mut input_recv) = input_event_stream();
        let (_output_send, output_recv) = output_event_stream();
        let ui = Box::new(Null);
        // let mut app =
        //     TermApp::new_from_ui(TestStdin(vec![1, 2, 3]), ui, input_send, output_recv).unwrap();
        // app.poll().await;
        // app.quit();
        assert_eq!(
            [
                input_recv.recv().await,
                input_recv.recv().await,
                input_recv.recv().await,
            ],
            [
                Some(InputEvent::Char(3)),
                Some(InputEvent::Char(2)),
                Some(InputEvent::Char(1)),
            ]
        );
    }
}
