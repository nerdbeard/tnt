use tnt::dm::{GameOver, DM};

use crate::app::TermApp;

mod app;

#[tokio::main(flavor = "current_thread")]
async fn main() {
    env_logger::init();
    loop {
        let mut dm = DM::new(0);
        let term = TermApp::new(std::io::stdin(), &mut dm.ui).unwrap();
        tnt::adventure::init(&mut dm);
        tnt::adventure::intro(&mut dm).await;
        match dm.game_loop().await {
            GameOver::Restarted => {
                dm.msg("\n  Oh, no.  Not again.\n\n[ RESTARTING! ]\n\n");
                dm.msg_flush().await;
                dm.reset_ui();
                term.quit();
            }
            GameOver::Quit => {
                dm.msg("\n\n[ QUITTING ]\n\n");
                dm.msg_flush().await;
                dm.reset_ui();
                term.quit();
                break;
            }
            unhandled => panic!("Unhandled GameOver: {unhandled:?}"),
        };
    }
    print!("\r");
}
